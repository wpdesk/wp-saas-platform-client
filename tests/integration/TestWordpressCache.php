<?php

class TestWordpressCache extends WP_UnitTestCase {

    const TEST_KEY = 'test_key';
    const TEST_VAL = 'test_val';

    const TEST_KEY2 = 'test_key2';
    const TEST_VAL2 = 'test_val2';

    private function getCache() {
        return new \WPDesk\SaasPlatformClient\Cache\WordpressCache();
    }

    public function testSet() {
        $cache = $this->getCache();

        $this->assertTrue($cache->set(self::TEST_KEY, self::TEST_VAL));
    }

    public function testGet() {
        $cache = $this->getCache();

        $cache->set(self::TEST_KEY, self::TEST_VAL);
        $this->assertEquals(self::TEST_VAL, $cache->get(self::TEST_KEY));
    }

    public function testDelete() {
        $cache = $this->getCache();

        $cache->set(self::TEST_KEY, self::TEST_VAL);
        $this->assertEquals(self::TEST_VAL, $cache->get(self::TEST_KEY));

        $cache->delete(self::TEST_KEY);
        $this->assertNull($cache->get(self::TEST_KEY), 'Item deleted. Value should be null!');
    }

    public function testClear() {
        $cache = $this->getCache();

        $cache->set(self::TEST_KEY, self::TEST_VAL);
        $cache->set(self::TEST_KEY2, self::TEST_VAL2);

        $this->assertTrue($cache->clear());
        $this->assertNull($cache->get(self::TEST_KEY), 'Value should be null after clear cache!');
        $this->assertNull($cache->get(self::TEST_KEY2), 'Value should be null after clear cache!');
    }

    public function testGetMultiple() {
        $cache = $this->getCache();

        $cache->set(self::TEST_KEY, self::TEST_VAL);
        $this->assertEquals(array(self::TEST_KEY => self::TEST_VAL), $cache->getMultiple(array(self::TEST_KEY)));
        $this->assertEquals(
            array(self::TEST_KEY => self::TEST_VAL, self::TEST_KEY2 => null),
            $cache->getMultiple(array(self::TEST_KEY, self::TEST_KEY2))
        );

        $cache->set(self::TEST_KEY2, self::TEST_VAL2);
        $this->assertEquals(
            array(self::TEST_KEY => self::TEST_VAL, self::TEST_KEY2 => self::TEST_VAL2),
            $cache->getMultiple(array(self::TEST_KEY, self::TEST_KEY2))
        );
    }

    public function testSetMultiple() {
        $cache = $this->getCache();

        $values = array(self::TEST_KEY => self::TEST_VAL, self::TEST_KEY2 => self::TEST_VAL2);

        $cache->setMultiple( $values );
        $this->assertEquals($values, $cache->getMultiple(array(self::TEST_KEY, self::TEST_KEY2)));
    }

    public function testDeleteMultiple() {
        $cache = $this->getCache();

        $values = array(self::TEST_KEY => self::TEST_VAL, self::TEST_KEY2 => self::TEST_VAL2);

        $cache->setMultiple( $values );
        $this->assertTrue($cache->deleteMultiple(array(self::TEST_KEY, self::TEST_KEY2)));
    }

    public function testHas() {
        $cache = $this->getCache();

        $cache->set(self::TEST_KEY, self::TEST_VAL);

        $this->assertTrue($cache->has(self::TEST_KEY), 'Item should exists in cache');

        $this->assertFalse($cache->has(self::TEST_KEY2), 'Item should not exists in cache');
    }
}