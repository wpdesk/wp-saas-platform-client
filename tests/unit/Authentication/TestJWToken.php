<?php

use WPDesk\SaasPlatformClient\Authentication\JWTToken;

class TestJWToken extends \PHPUnit\Framework\TestCase
{

    public function testExpiredToken()
    {
        $expiredToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1MzQ4NTc2OTEsImV4cCI6MTUzNDg2MTI5MSwicm9sZXMiOlsiUk9MRV9VU0VSIiwiUk9MRV9TSE9QIl0sInVzZXJuYW1lIjozLCJpcCI6IjE3Mi4xOS4wLjEiLCJzaG9wIjoxfQ.iB9u8f3uBdq1KhkkYPAVpPFSwvkK4CQypfFQIiM8N5acQSFxv-jyzD2guGs4HtLOdMvu4Dt5zkd4ZfQFJNT2b6k7i33FyU4AsEDAVtwHL-TcqsnomXn70CjB5Rhgd2LteFl6wPp3XonE4SZ6Oo3vtBZzSoNNkR6-7T3OseJMwoJ7qzlEFBixNXG6UTlXPJky_b-rbfhFORlInxVzvs4GJgkJM3F3Ugy4bLSSPtBYxWcwKnKkFE8L4Z87Pezp4v35aXqJFpvJ3zll0gJ1F32Z2vx1oCmu0jOHkzzmu3wA2u6gK0iNgp591M7MKH2_3HaLfFY06cLoLDN6TR6wmzSvZYmzSw8C68MUH7uGWyGRU9j4YtdL8Bom3v8D1J-IC4Jx6-QPE665nd1VgzHZb1TFkHseUx3kLF5Jhgq7095NJ79QTC-6XTW2bN-T-dbbFkvjCU-B-9Ti09uMUEn4Rtlt_lThbr_lA9Wyc4qXAecqCz6dAC2UTy-_KxLwvfNrQ4sSS4y3B8bPh8qJrI0EsIbi5nY20sgBT71abUG9DHmIzj4rA5YW_DOu7Cez0WhNoCHjhMEymATrD2cWYxFCPWQfTTBuoD1HZgVfwYI9B-aZPw669orNRGkBtP8Bm5ghmCZoRHpISiY7UoIzgOnVKhB-Qv43g1uRprk7nYnfVEglqXg';

        $jwtToken = new JWTToken($expiredToken);
        $this->assertTrue($jwtToken->isSignatureValid(), 'Signature should be valid');
        $this->assertTrue($jwtToken->isExpired(), 'Token should be expired long ago');
    }

    public function testImmortalToken()
    {
        $tokenWithExpiredDateInFuture = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1MzQ5NDIyODgsImV4cCI6MzI1MDM2ODAwMDAsInJvbGVzIjpbIlJPTEVfVVNFUiIsIlJPTEVfU0hPUCJdLCJ1c2VybmFtZSI6MywiaXAiOiIxNzIuMTkuMC4xIiwic2hvcCI6NX0.gWsHsCGm8iUc13lGc9PgUtwv-qIYWAl98-jdzOdzuQ_PwreLuEFonP-hutp0_IXZtHeE6e1XLID-Cgn0YaahLrxybVGBTbcVgTJVOMB9Fv-gP9lHYNqvBdfQRcdXiO6PYCBbfpezunhcmiML_ebFfprdoMn8kG3K-XbEkyRB7MOQg0-dZ35tncVCZDfzLh5fucFzteQmCcddIosKpqr-rjYjRRCAB-aTE1vbAulZa1_VmP17l6m64__QBjHW9r07OTq7QviayXpOB_4mBdxI26XjgDXANCzlejcka5Uh7AS03cP3zDP8Mc0VaVZxqUhwotQ93s0VtyMNyUn_nkMebAuwXkDOmgsQzJHhlPD8hs9vzmNbN6ymKwsSZ-E3q1yPTQwXzu-rERdCaNifAIKlUyTETlvE3aoN7M0JjdCIVHBkf81ygppMITYJ0eytjyqBg0wjdRlMCpHEtGVosqLe8sNZo7NeZ9tPexhZxWXQNRA6VyE1NwDYd9yPsKBSPrulFcvkgoGOuL98rfE9iizMDYD60G0eTgafnR_99-IH4yoBMIjf4UNVzAcw8revbSE-3bgmxpdjVTJtVXw9LH13BHdB6GN7KJDwH4NRoz_MQJMSBELU18zY2yjmkNLXYI3YArTE3OQ-qBzD7rgPrcJBmoWTswbNVnTl2csFVHdQZOo';

        $jwtToken = new JWTToken($tokenWithExpiredDateInFuture);
        $this->assertTrue($jwtToken->isSignatureValid(), 'Signature should be valid');
        $this->assertFalse($jwtToken->isExpired(), 'Token should NOT be expired. Have 3000 years of live');
    }

    public function testStringValueAndBearer() {
        $someToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1MzQ5NDIyODgsImV4cCI6MzI1MDM2ODAwMDAsInJvbGVzIjpbIlJPTEVfVVNFUiIsIlJPTEVfU0hPUCJdLCJ1c2VybmFtZSI6MywiaXAiOiIxNzIuMTkuMC4xIiwic2hvcCI6NX0.gWsHsCGm8iUc13lGc9PgUtwv-qIYWAl98-jdzOdzuQ_PwreLuEFonP-hutp0_IXZtHeE6e1XLID-Cgn0YaahLrxybVGBTbcVgTJVOMB9Fv-gP9lHYNqvBdfQRcdXiO6PYCBbfpezunhcmiML_ebFfprdoMn8kG3K-XbEkyRB7MOQg0-dZ35tncVCZDfzLh5fucFzteQmCcddIosKpqr-rjYjRRCAB-aTE1vbAulZa1_VmP17l6m64__QBjHW9r07OTq7QviayXpOB_4mBdxI26XjgDXANCzlejcka5Uh7AS03cP3zDP8Mc0VaVZxqUhwotQ93s0VtyMNyUn_nkMebAuwXkDOmgsQzJHhlPD8hs9vzmNbN6ymKwsSZ-E3q1yPTQwXzu-rERdCaNifAIKlUyTETlvE3aoN7M0JjdCIVHBkf81ygppMITYJ0eytjyqBg0wjdRlMCpHEtGVosqLe8sNZo7NeZ9tPexhZxWXQNRA6VyE1NwDYd9yPsKBSPrulFcvkgoGOuL98rfE9iizMDYD60G0eTgafnR_99-IH4yoBMIjf4UNVzAcw8revbSE-3bgmxpdjVTJtVXw9LH13BHdB6GN7KJDwH4NRoz_MQJMSBELU18zY2yjmkNLXYI3YArTE3OQ-qBzD7rgPrcJBmoWTswbNVnTl2csFVHdQZOo';

        $jwtToken = new JWTToken($someToken);
        $this->assertEquals($jwtToken->__toString(), $someToken);
        $this->assertEquals($jwtToken->getAuthString(), 'Bearer ' . $someToken);
    }
}