<?php

use WPDesk\SaasPlatformClient\Authentication\JWTSaasToken;
use WPDesk\SaasPlatformClient\Authentication\JWTToken;

class TestJWTSaasToken extends \PHPUnit\Framework\TestCase
{
    /**
     * Provides tokens with info about expected shop id encoded inside
     *
     * @return array
     */
    public function provideTokensWithShopInfo()
    {
        return [
            [
                'token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1MzQ5NDA2NjQsImV4cCI6MTUzNDk0NDI2NCwicm9sZXMiOlsiUk9MRV9VU0VSIiwiUk9MRV9TSE9QIl0sInVzZXJuYW1lIjozLCJpcCI6IjE3Mi4xOS4wLjEiLCJzaG9wIjoxfQ.rG6_U67zKTinkqR4324wOhS0YP9P9_2DH-OMsfijTPt408wNGwh5YKqkA4kglP5wMQQ80UGo7qyYd9R3fc465JMB9DQTbALATz_UMk8fr_-LWl9Gj8cuzQvEMQL2Saya_nggAn0bmWlVG27k6-6ezfBW7Hhb1d1GtoydbiYMA_ntLVPzrRjicvcIoftroadX6rsuCSHy-lLvHd1pj6E6eOEX2IcZXoa9eqd_3-j4dgszNUoPDag8bYXQzmIDIqyseBQ3eaQr69Tj-npTaMDjSyHwFQqtr57leX9aVVo8xPeF2ulrT4u4VgCv6kQt1Fao5_G6WLJ9FpJE19k_e4uebSwmDwiPoSnqU30tmIoVpcqfzxoNDKxfaK6am4HvfiQ6l_A2Cb7Wi9Wh5Px1A2a1NjFNKq-hxI3bVb30cKvacL29tYcjT-315RKv57onnCjVJWaGfzPJ44ZibB2VaVF9HO8Rjci8PJ35lF0NOw7YYb7rTQB0JOsLhHrwe9cjgKBsxwIAOVx1fFt6fq1RRJzUpcHKAx0f7kwGmHzXNwpNIexneBYg1Btz5UfW3yiztxggBPYvDfFAdLc5hfFURFhLtqaQzCklomRYBkbEkBGSt_5iz7HLYiiWuM51TzXgUMa631KwCuyBA675fwEMnwQ01YM51Et7NQgRcja737kSliM',
                'shop' => 1
            ],
            [
                'token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1MzQ5NDEyMDAsImV4cCI6MTUzNDk0NDgwMCwicm9sZXMiOlsiUk9MRV9VU0VSIiwiUk9MRV9TSE9QIl0sInVzZXJuYW1lIjozLCJpcCI6IjE3Mi4xOS4wLjEiLCJzaG9wIjo1fQ.NAh6p40uSgyhcRkXxLSJhsGSn-mRAIYGsbtuMqSrplHk7LxrOQvvLBKfW0cZpl4y683ECGYqykJYmGb3nnWPKOQv5Pl2GExLItvzWuVju6KvkTi-_RD1xVXv6tCxkI6umcrJaI0bUWQhjLogrjNgIAR81DRbfG81lctIRzhP-CtADD9J7uEFrfWIGIbBgVOMdiw4nsAvqB9ECTDbnv5TRsxmOnyhm42mWxfpbrzd7WaRugIMWbysL2_5HXwqLV7jaaPlBHFpGs3cpgpRF0fsZ1VZeM7LAiiwvNuBwVsZbmD_ZKKdXzwL183RbsuSP4C_7gjH4UXEflRWRR6nzom_AyCFhqGrdu1obSNLiIeIt94p_VavOmwicwQKmuhOHGnYHVguTLYW2FcQIS2nMosswnXoZzTJcL3YwQQsMT6Xg-Da43bWZMIWvcK7d-nSvOGHW0OrHzWQRZOcH3RKANzPUEG5L0-KL55yacMbAPq6ykGc-I-_q1IDBxfxE2rSaxGKa72O60uwjX2Mafdhp9YWuESNm2Gvi09hYo3_dBUAHjaSdXOWqJhb8h1ND8c2IJNxltTIukz1fDdLkFvTttZi4g5rjsJoDN94Y3f-19qokY8lzhx8z3Zc4hgbNQ9p8CYrmjB6FDa59Psuxbd-ODwzNm_uLsn1z4U5uo68QHa5g50',
                'shop' => 5
            ],
            [ // case with wpdesk and shop role
                'token' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1MzQ5NDE0MzYsImV4cCI6MTUzNDk0NTAzNiwicm9sZXMiOlsiUk9MRV9XUERFU0siLCJST0xFX1NIT1AiXSwidXNlcm5hbWUiOjEsImlwIjoiMTcyLjE5LjAuMSIsInNob3AiOjJ9.pD9qQQJB_Mn6hSBcK5unEyrQ2n-3BgnzCRtk1V_u7BL0SKqgqILQN5NGJYpG2dO1Rb54pCNgAVzr6axu9f6ijmGetpHX_V1HSy7HOM4xdI8H7BaPHaK-2KUKTrVFM7lEp_sRc4KBkfSW4Ju2EH0e0Oty42EkIWeTb3J3npTHK_RjXI47xX5e_gZW8inRfJx24EhiSnlkMX77lIpXsqqCyRKqW64niLrQzKMNeSOJ_5HgBvzk9OyR9H5O8_s8EAliD24LvynoxgQOp9r0A78EIBRoqdZKJSS7kSgl_8eRFTGgxZBPGmyBKNWvxrd2XnRp19QPgymMi2kR0HJDWRemtSFHxYsjTrVVnpeiHjZ7WTKNawZPB-7ELPIHm_C_-GuJ4I1fnnM_uUna2rh6mr3KeRQF4LJGmDjesdHIpy8FDmepqsIcAAxpmAi6kJRtDr6QAxoh20zi0jCni1pPmLQyoDsau7U7l3Nev_STFbFiHBk-mLYbhNqV_MCcFY1-GeWynfehehWoVeGTShVoxXrDSWklPb58DaQKuwXJ8-kvmvWVhVqWGqF5l7qUj3D2CIxr4tXsQ6FUPkntvFw3S4GrLqAl-P7XoR5alRxSyC6KypYeY7pHRZBDDeoFWgCOJhEymWtKrcTt-Yzd2EL3vQYnwhYOp0hw1n-eXnE1WkaB4Wg',
                'shop' => 2
            ]
        ];
    }

    /**
     * Provides tokens when can't get info about shop
     *
     * @return array
     */
    public function provideTokensWithInvalidShopInfo()
    {
        return [
            ['invalid' => 'siUk9MRV9XUERFU0siLCJST0xFX1NIT1AiXSwidXNlcm5hbWUiOjEsImlwIjoiMTcyLjE5LjAuMSIsInNob3AiOjJ9.qeroBbMHVgQWypDaF6FPmaZadu7D1u1JObCn9rXy44pGL1mWESdVCI6pU_MajeCJsQ-V3emWMOfqHl5AIiH7BZb2iW04ahGJjJTtRGaEfGH5ztyG-00TVzf51V0mtX3i3DuEEJd8F6LDHK8C51oqm6WkHVv__GhsqOlMz_WTDdst-IRUK-kgcUBhbYy-XzVxt4LADQhs4SsJqd-0wYPaobTFRQaHel69oGF_ymwJ4snla4uJxlfIHLtmYRArOIcDNKcPIOH8cMo-UJP9B15IxuOGO0M2hR74LNSmDUpIHmeQ24rEiwp73q2yhXYIHhIBdExDk7GAHFhOILIqa05D48V8fkPLr0TFVVCACvylJHwj3UsYPzI1WvhdrEHcGr1MC8bWB2KAC_RpB5Vvo7kPp0YXcUa7nbszL90v1njKpVrcSrpYG69a4Ym8ZXSdAS7jJmoSH-Xf3JXXi_6vtxtSsm-DG_zwd7VoWupBu1rG1h_ARW-Lambj_Ql2XZEAWAvdR6LqHkL5SyIZ7CnscbNY_VbYoyRHsQIwhzbaQdBSwBa4qu0VsEHtwdV2acbDZhojm-DMb4b7vr3znyw_WnHPSvjOGRXmW2KhD0bwwfrAJcquc0rL5N04FRVYXR7AnJjN-j1j0bfhvDN9a5W-yHidmp9_OZ0HVCqK5c0IIsiv-Ms'],
            ['no shop id' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1MzQ5NDE5MjMsImV4cCI6MTUzNDk0NTUyMywicm9sZXMiOlsiUk9MRV9VU0VSIiwiUk9MRV9TSE9QIl0sInVzZXJuYW1lIjozLCJpcCI6IjE3Mi4xOS4wLjEifQ.S8Dkz9C7d0zHRn5_uRzjdBHH437JJZ4CdHTRc-Bwn1bnP9rj0SGn3AbCA53TlZjBl2EX0cigRlEMYsKEr0eQdQK8dellP0EjtPSaX5_5nEuU_H0X_6oJT8wYiS9N2jVm6W3UiKHMJBCR9JQfhjHYmMRx-NKhAi5xUgiGIWC7IWhNvLzHu52sRozkG2jTssnHGid087FdnQ1aG9c4dYY9piSw-5xBf8FruP9sDoWhTvFzESHIlwzgL5MNHQx-9HUH6caWoblpas959YqbD29h49YuCHLbifR1sMjSUWnkw69WLGDAFs8aKPCR5FsfKg3Rac0s957Cl3ES60eL7kWJ2mk1kRjxpFa6me1pWvh7Pt6lpDsJPLvTIbcZ_aDX5taOOiucL8YO-PbCB37Ffi2L4Am0-b4EGrqarBprjUWe2CJA9tCfjjFm0McOotDNVjLDmU7wAdHv8hXEOq4Kr2wd1z7suRqZii3VjMotoWFz9Cq4BTu1cs_Z_OLq5aC_uhnWq-SsKx3pdfL5tDXZ_CPqDCW0_3692FT8Z_puk1UUtMI_I_GkY_gp-npH72D8VcYdI-0j4LJoEtviGk6-0Lk47CiygGw79TaFjcEhwxPoYD_7MwMEw7vJiYYO8PVptlSuQy7j2-cPI0XPHlu19tVzA97ilqOlRAxt_SihXMfSoyo'],
            ['no shop role' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1MzQ5NDIwNjEsImV4cCI6MTUzNDk0NTY2MSwicm9sZXMiOlsiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjozLCJpcCI6IjE3Mi4xOS4wLjEiLCJzaG9wIjo1fQ.PS5SQixw9k9_-Waxho4GKJ1QudCSzee6gy9_cFjWnVcBpMGH-PkHf5iDO5rWwqQSsKpBgMi5m-FFCYoelHa5q1-XAn6FkRlj0aXNcQZCY6YGfqIaDeCP-0DEVoSqfaceFXLZbTyPz4aKrurONjWdZwuw9uull6Ho4Nq1B_E5QTpJBYehmebgWnS5OjHDjJVW0UrknwKHiRSohP-vCA5P2HQGZpZXrUCkLFcL2PqCCchORuHUQr3xjn2OleE4Jt1RsD5Lymo3m8s6WkRw9GepWV7-vJ0WDh3Mu_irkstZlCyujfh0PNnRSEyr9jshW2OXnDX_l0dLzsd5n6pPX1OVa58FpkZNb3s7hAsVxQ2Dd9GUg2o-qMZDCP9LbXn_CGUqiyXo4iIdal4balF0DpUGNEfggwqnL4leTsHqUirsN1bhLLhRmkAYooCfFuEMA9F5G66zF8Vwl5AuZSz8Nrj-T0krM54H5ykhzVttpWyRA9TcVywKAC9E2DFDOMfajtJA_LcMv5xg-m0uhhxlGk25HAY8yVWQdPfBp5KIq2jO3vLPzVmBDPFjtDgwiUXvXVakFS7AmtI8DRWh8dq92F2dGDV67uZ_WK5kqBQEtO1abfM5EF0zcQ9r_E4KqjLJfXyCyLMucw8CUdsE8MEcCfjpE06_mSuRMW1dpoSPJWGXE5Y'],
            ['no whole shop info' => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJpYXQiOjE1MzQ5NDIwMjksImV4cCI6MTUzNDk0NTYyOSwicm9sZXMiOlsiUk9MRV9VU0VSIl0sInVzZXJuYW1lIjozLCJpcCI6IjE3Mi4xOS4wLjEifQ.XTvcWiKWLda4TnHhWnqrOKCBseiHmjnDYgmPCKhX6NrEYMu4mN0puKgnoeOWJVZHBjFQ5X--khMFKNKU4e7Zgc6MWgnSc5niKyZpr73D-PVfUkOG81we2-DSteik2i3F2R2cTSIUbYMJKeqe7fQ0qTZ3-7s6kNgKAkIhGraQrbbxi-FTp4uNbUrDwcDEyBZ_T9zmcR9jXsKLVJszHdmc8K_eVYwj2jOR8bouGOqpCHwlGz0bEpcSPqXJ_DsotVeM91QIBls4JzImTFbYr06N1GdG4oFzMNWqVhO9iJ4-ElN_COPLp1ou0i1Ha0ZedYajI2CJrhailbF5ybZd5x08tiDoMa26aVpH09wOe_P4zYno0ksMnZ66WpPcnBgnPA_dlgcqUUkc93igicD8XCKEBokEyF16IzI3A8Y85FEuwkIj-JadM8ZTJqHErxNbhZyuEGqWvQfwhawade-3CxOUz3krbG8LxhNPBYrm2t8NQpRwwFoQPa0lvEdMiGo4JL0J7hW02shT2iGPOUwJ0_KfpXCU5ensd60Mou0GQhnUPhKvnA0CtvQn5JgX_TM5OKKCSvtcIEaW2pqq96--S8atnQp2GwZU0Ccs8R6HAfH61d8cwL8abwKvr6A5wQoskn28YPwWu8g9Tvt556i2oIvFWEjrFOnfQyhmQj1GCVMafFc'],
        ];
    }

    /**
     * @dataProvider provideTokensWithShopInfo
     */
    public function testValidShopInfoFromToken($token, $shop)
    {
        $saasToken = new JWTSaasToken(new JWTToken($token));
        $this->assertTrue($saasToken->hasShopId(), 'Token should have shop id info');
        $this->assertEquals($shop, $saasToken->getShopId(), 'Invalid shop id provided in token');
    }

    /**
     * @dataProvider provideTokensWithInvalidShopInfo
     */
    public function testTokensWithInvalidShopInfo($token)
    {
        $saasToken = new JWTSaasToken(new JWTToken($token));
        $this->assertFalse($saasToken->hasShopId(), 'Token should NOT have shop id info');
    }
}