<?php

use WPDesk\SaasPlatformClient\Authentication\Token;
use WPDesk\SaasPlatformClient\Request\Fields\GetFieldsRequest;
use WPDesk\SaasPlatformClient\Request\Label\PostLabelRequest;

class TestGetFieldsRequest extends \PHPUnit\Framework\TestCase
{


    public function testEndpoint()
    {

        $shopId = 1;
        $serviceId = 2;
        $zoneTargets = ['PL', 'UK'];

        $token = $this->createMock(Token::class);

        $fieldsRequest = new GetFieldsRequest(
            $token,
            $serviceId,
            $shopId,
            $zoneTargets
        );

        $serializedZoneTargets = implode(',', $zoneTargets);

        $this->assertEquals("/shops/{$shopId}/shipping_services/{$serviceId}/dynamic_fields/{$serializedZoneTargets}",
            $fieldsRequest->getEndpoint(), 'Endpoint is not properly generated');
    }
}