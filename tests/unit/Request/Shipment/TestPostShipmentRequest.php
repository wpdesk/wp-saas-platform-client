<?php

use WPDesk\SaasPlatformClient\Authentication\Token;
use WPDesk\SaasPlatformClient\Model\Shipment\ShipmentRequest;
use WPDesk\SaasPlatformClient\Request\Shipment\PostShipmentRequest;

class TestPostShipmentRequest extends \PHPUnit\Framework\TestCase
{


    public function testEndpoint()
    {

        $shopId = 1;
        $serviceId = 2;

        $token = $this->createMock(Token::class);

        $shipmentRequest = new ShipmentRequest();

        $shipmentRequest = new PostShipmentRequest(
            $token,
            $serviceId,
            $shopId,
            $shipmentRequest
        );

        $this->assertEquals("/shops/{$shopId}/shipping_services/{$serviceId}/shipments", $shipmentRequest->getEndpoint());
    }
}