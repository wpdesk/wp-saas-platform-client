<?php

use WPDesk\SaasPlatformClient\Authentication\Token;
use WPDesk\SaasPlatformClient\Request\ShippingPlan\PutShippingPlanRequest;

class TestPutShippingPlanRequest extends \PHPUnit\Framework\TestCase
{
    public function testEndpoint()
    {
        $token = $this->createMock(Token::class);
        $plan = new \WPDesk\SaasPlatformClient\Model\ShippingPlan([
            'id' => 5
        ]);

        $putPlanRequest = new PutShippingPlanRequest(
            $token,
            $plan
        );

        $this->assertEquals("/shipping_plans/5", $putPlanRequest->getEndpoint());
    }
}