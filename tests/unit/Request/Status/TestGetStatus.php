<?php

use WPDesk\SaasPlatformClient\Authentication\Token;
use \WPDesk\SaasPlatformClient\Request\Status\GetStatusRequest;

class TestGetStatus extends \PHPUnit\Framework\TestCase
{


    public function testEndpoint()
    {

        $shopId = 1;
        $serviceId = 2;
        $zoneTargets = ['PL', 'UK'];

        $token = $this->createMock(Token::class);

        $fieldsRequest = new GetStatusRequest($token);

        $this->assertEquals(
            "/status",
            $fieldsRequest->getEndpoint(),
            'Endpoint is not properly generated'
        );
    }
}