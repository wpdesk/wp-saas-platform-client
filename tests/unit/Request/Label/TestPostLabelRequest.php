<?php

use WPDesk\SaasPlatformClient\Authentication\Token;
use WPDesk\SaasPlatformClient\Request\Label\PostLabelRequest;

class TestPostLabelRequest extends \PHPUnit\Framework\TestCase
{


    public function testEndpoint()
    {

        $shopId = 1;
        $serviceId = 2;
        $shipmentId = 3;

        $token = $this->createMock(Token::class);

        $labelRequest = new PostLabelRequest(
            $token,
            $serviceId,
            $shopId,
            $shipmentId
        );

        $this->assertEquals("/shops/{$shopId}/shipping_services/{$serviceId}/shipments/{$shipmentId}/labels", $labelRequest->getEndpoint());
    }
}