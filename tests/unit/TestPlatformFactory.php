<?php

use WPDesk\SaasPlatformClient\Platform;
use WPDesk\SaasPlatformClient\PlatformFactory;
use WPDesk\SaasPlatformClient\PlatformFactoryOptions;

class TestPlatformFactory extends \PHPUnit\Framework\TestCase
{
    public function testCreation()
    {
        $platform = PlatformFactory::createPlatformApi(new PlatformFactoryOptions());
        $this->assertInstanceOf(Platform::class, $platform);
    }
}