<?php

use WPDesk\SaasPlatformClient\Authentication\JWTToken;
use WPDesk\SaasPlatformClient\HttpClient\HttpClient;
use WPDesk\SaasPlatformClient\HttpClient\HttpClientResponse;
use WPDesk\SaasPlatformClient\PlatformFactory;
use WPDesk\SaasPlatformClient\PlatformFactoryOptions;

class TestPlatform extends \PHPUnit\Framework\TestCase
{
    const EXAMPLE_EMAIL = 'login@example.com';
    const EXAMPLE_INVALID_EMAIL = 'invalid';

    const EXAMPLE_DOMAIN = 'wp.pl';

    const LOGIN_SUCCESS_CODE = 200;
    const CREATED_HTTP_CODE = 201;
    const TOKEN_EXPIRED_CODE = 401;
    const BAD_REQUEST_CODE = 400;
    const REQUEST_SUCCESS_CODE = 200;

    const PASSWORD_FIELD = 'password';
    const TOKEN_FIELD = 'token';

    const PASSWORD_VALUE = 'whateverPassword';
    const TOKEN_VALUE = 'whateverToken';

    /** @var  string */
    private $email;

    /** @var  string */
    private $domain;

    /** @var  string */
    private $apiKey;

    /** @var string */
    private $locale;

    /** @var  \WPDesk\SaasPlatformClient\Platform */
    private $platform;

    protected function setUp()
    {
        $options = new PlatformFactoryOptions();

        $this->platform = PlatformFactory::createPlatformApi($options);

        $this->email = self::EXAMPLE_EMAIL . md5(uniqid('', true));
        $this->domain = self::EXAMPLE_DOMAIN . md5(uniqid('', true));
        $this->apiKey = md5(uniqid('', true));
        $this->locale = 'en_GB';
    }

    public function testRegisterSuccess()
    {
        $validResponse = json_encode([], JSON_FORCE_OBJECT);

        $httpClientMock = Mockery::mock(HttpClient::class);
        $httpClientMock
            ->shouldReceive('send')
            ->withAnyArgs()
            ->andReturn(new HttpClientResponse('[]', $validResponse, self::CREATED_HTTP_CODE));

        /** @var HttpClient $httpClientMock */
        $this->platform->getClient()->setHttpClient($httpClientMock);

        $response = $this->platform->requestRegister($this->email, $this->domain, $this->locale);

        $this->assertTrue($response->isUserRegistered(), 'User should be registered here');
    }

    public function testRegisterFailed()
    {
        $httpClientMock = Mockery::mock(HttpClient::class);
        $httpClientMock
            ->shouldReceive('send')
            ->withAnyArgs()
            ->andReturn(new HttpClientResponse('[]', '{}', self::BAD_REQUEST_CODE));

        /** @var HttpClient $httpClientMock */
        $this->platform->getClient()->setHttpClient($httpClientMock);

        $response = $this->platform->requestRegister(self::EXAMPLE_INVALID_EMAIL, self::EXAMPLE_DOMAIN, $this->locale);
        $this->assertFalse($response->isUserRegistered(), 'Email was invalid so user should not be registered here');
    }

    public function testTokenSuccess()
    {
        $validResponse = json_encode([
            self::TOKEN_FIELD => self::TOKEN_VALUE
        ], JSON_FORCE_OBJECT);

        $httpClientMock = Mockery::mock(HttpClient::class);
        $httpClientMock
            ->shouldReceive('send')
            ->withAnyArgs()
            ->andReturn(new HttpClientResponse('[]', $validResponse, self::LOGIN_SUCCESS_CODE));

        /** @var HttpClient $httpClientMock */
        $this->platform->getClient()->setHttpClient($httpClientMock);

        $response = $this->platform->requestToken($this->apiKey, $this->domain, $this->locale);
        $jwtToken = $response->getToken();
        $this->assertInstanceOf(JWTToken::class, $jwtToken);
        $this->assertNotEmpty($jwtToken->__toString(), 'Token should have value');
        $this->assertEquals($jwtToken->__toString(), self::TOKEN_VALUE, 'Token should have mocked value');
    }

    public function testTokenExpiredAndRelogin()
    {
        $this->testTokenSuccess();

        $expiredResponse = json_encode([
            'code' => 401
        ], JSON_FORCE_OBJECT);

        $validTokenResponse = json_encode([
            self::TOKEN_FIELD => self::TOKEN_VALUE
        ], JSON_FORCE_OBJECT);

        $httpClientMock = Mockery::mock(HttpClient::class);
        $httpClientMock
            ->shouldReceive('send')
            ->andReturnUsing(function ($url) use ($expiredResponse, $validTokenResponse) {
                static $count = 0;
                $count++;

                // returns invalid token on first shipping service request
                if ($count === 1 && strpos($url, 'shipping_services') !== false) {
                    return new HttpClientResponse('[]', $expiredResponse, self::TOKEN_EXPIRED_CODE);
                }
                // return valid token on relogin
                if (strpos($url, 'token') !== false) {
                    return new HttpClientResponse('[]', $validTokenResponse, self::LOGIN_SUCCESS_CODE);
                }

                // return valid request after all this mess
                return new HttpClientResponse('[]', '[]', self::REQUEST_SUCCESS_CODE);
            });

        /** @var HttpClient $httpClientMock */
        $this->platform->getClient()->setHttpClient($httpClientMock);

        $response = $this->platform->requestListShippingServices();
        $this->assertEquals(self::REQUEST_SUCCESS_CODE, $response->getResponseCode(),
            'Request should be success after tolen relogin');
    }

}