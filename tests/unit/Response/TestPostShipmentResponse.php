<?php

use WPDesk\SaasPlatformClient\Response\RawResponse;
use WPDesk\SaasPlatformClient\Response\Rate\PostRateResponse;
use WPDesk\SaasPlatformClient\Response\Shipment\PostShipmentResponse;

class TestPostShipmentResponse extends \PHPUnit\Framework\TestCase
{
    public function testShipmentResponseWithoutCost()
    {
        $rawPostShipmentResponse = '{
            "shipmentId":298,
            "status":"confirmed",
            "message":"Success",
            "messageStack":[],
            "trackingId":"800030215308624",
            "trackingUrl":null,
            "shipmentCountSent":8,
            "shipmentCountLeft":42,
            "shipmentCost":null
        }';

        $shipmentResponse = new PostShipmentResponse(new RawResponse(json_decode($rawPostShipmentResponse, true), 200, array()));

        $shipment = $shipmentResponse->getShipment();
        $this->assertEquals(298, $shipment->getShipmentId(), "Data should correspond the json");
        $this->assertEquals( 'Success', $shipment->getMessage(), "Data should correspond the json");
        $this->assertEquals( 'confirmed', $shipment->getStatus(), "Data should correspond the json");
        $this->assertFalse( $shipment->hasShipmentCost(), "Data should correspond the json");
        $this->assertEquals(null, $shipment->getShipmentCost(), "Data should correspond the json");
    }
}