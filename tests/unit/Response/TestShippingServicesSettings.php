<?php

use WPDesk\SaasPlatformClient\Authentication\JWTToken;
use WPDesk\SaasPlatformClient\HttpClient\HttpClient;
use WPDesk\SaasPlatformClient\Model\ShippingServiceSetting;
use WPDesk\SaasPlatformClient\PlatformFactory;
use WPDesk\SaasPlatformClient\PlatformFactoryOptions;

class TestShippingServicesSettings extends \PHPUnit\Framework\TestCase
{

    /** @var \WPDesk\SaasPlatformClient\Platform */
    private $platform;

    /** @var int */
    private $shopId;

    private function preparePlatform()
    {
        $options = new PlatformFactoryOptions();
        $this->platform = PlatformFactory::createPlatformApi($options);
    }

    protected function setUp()
    {
        $this->shopId = 1;
        $this->preparePlatform();
        $tokenMock = Mockery::mock(JWTToken::class);
        $tokenMock->shouldReceive('isExpired')->andReturnFalse();
        $tokenMock->shouldReceive('isSignatureValid')->andReturnTrue();
        $tokenMock->shouldReceive('getAuthString')->andReturns('Bearer: whatever');
        $tokenMock->shouldReceive('getDecodedPublicTokenInfo')->andReturns(['shop' => $this->shopId, 'roles' => ['ROLE_SHOP']]);


        /** @var JWTToken $tokenMock */
        $this->platform->setToken($tokenMock);
    }

    public function testGetItem()
    {
        $shippingService = '/api/v1/shipping_services/1';
        $shop = '/api/v1/shops/1';
        $jsonValue = '{
                "account_number": "x",
                "username": "ya",
                "password": "z",
                "key": "aa"
            }';

        $rawListResponse = '{
            "_links": {
                "self": {
                    "href": "/api/v1/shipping_services/settings/shippingService=1;shop=1;type=connection"
                },
                "shipping_service": {
                    "href": "' . $shippingService . '"
                },
                "shop": {
                    "href": "' . $shop . '"
                }
            },
            "json_value": ' . $jsonValue . ',
            "type": "connection"
        }';

        $httpClientMock = Mockery::mock(HttpClient::class);
        $httpClientMock
            ->shouldReceive('send')
            ->withAnyArgs()
            ->andReturn(new WPDesk\SaasPlatformClient\HttpClient\HttpClientResponse('[]',
                $rawListResponse, 200));
        /** @var HttpClient $httpClientMock */
        $this->platform->getClient()->setHttpClient($httpClientMock);;

        $settingsResponse = $this->platform->requestGetSettings(1);
        $shippingServiceSetting = $settingsResponse->getSetting();
        $this->assertInstanceOf(ShippingServiceSetting::class, $shippingServiceSetting, 'Settings should be hydrated');
        $this->assertEquals($shop, $shippingServiceSetting->getShop(), 'Shop should be hydrated with valid value');
        $this->assertEquals($shippingService, $shippingServiceSetting->getShippingService(), 'Shop should be hydrated with valid value');
        $this->assertArraySubset(json_decode($jsonValue, true), $shippingServiceSetting->getJsonValue(), 'Field values should be equal');
    }

    public function testPostItem()
    {
        $shippingServiceSetting = new shippingServiceSetting();
        $shippingServiceSetting->setJsonValue(json_decode('{ "account_number": "x", "username": "ya", "password": "z", "key": "aa" }', true));
        $shippingServiceSetting->setType('sometype');
        $shippingServiceSetting->setShippingService(12345);

        $httpClientMock = Mockery::mock(HttpClient::class);
        $httpClientMock
            ->shouldReceive('send')
            ->withArgs(function($url, $method, $data) {
                return $method === 'GET';
            })
            ->andReturn(new WPDesk\SaasPlatformClient\HttpClient\HttpClientResponse('[]','[]', 200));

        $httpClientMock
            ->shouldReceive('send')
            ->withArgs(function($url, $method, $data) use ($shippingServiceSetting) {
                $expectedData = json_encode([
                    'json_value' => $shippingServiceSetting->getJsonValue(),
                    'shipping_service' => $shippingServiceSetting->getShippingService(),
                    'shop' => $this->shopId,
                    'type' => $shippingServiceSetting->getType(),
                ]);

                $this->assertEquals($expectedData, $data, 'Invalid data serialization in put/post. Serialization not match.');

                return $method === 'PUT';
            })
            ->andReturn(new WPDesk\SaasPlatformClient\HttpClient\HttpClientResponse('[]','[]', 200));

        /** @var HttpClient $httpClientMock */
        $this->platform->getClient()->setHttpClient($httpClientMock);;

        $this->platform->requestSaveSettings($shippingServiceSetting);
    }
}