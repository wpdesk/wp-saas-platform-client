<?php

use WPDesk\SaasPlatformClient\Response\RawResponse;
use WPDesk\SaasPlatformClient\Response\Rate\PostRateResponse;

class TestRate extends \PHPUnit\Framework\TestCase
{
    public function testRateResponse()
    {
        $rawRateResponse = '{
            "rates": [
                {
                    "serviceType": "03",
                    "serviceName": "UPS Ground",
                    "transportCharge": {
                        "amount": "3861",
                        "currency": "PLN"
                    },
                    "serviceCharge": {
                        "amount": "0",
                        "currency": "PLN"
                    },
                    "totalCharge": {
                        "amount": "3861",
                        "currency": "PLN"
                    },
                    "daysToDelivery": null
                }
            ],
            "messageStack": [
                "message one"
            ]
        }';

        $rateResponse = new PostRateResponse(new RawResponse(json_decode($rawRateResponse, true), 200, array()));

        $rate = $rateResponse->getRate();
        $this->assertAttributeCount(1, 'rates', $rate, "That response should have one rate");
        $this->assertEquals(1, count($rate->messageStack), "That response should have one message in stack");
        foreach ($rate->rates as $singleRate) {
            $this->assertEquals($singleRate->serviceName, 'UPS Ground', 'ServiceName in this mock should be UPS Ground');

            $totalCharge = $singleRate->totalCharge;
            $this->assertEquals($totalCharge->getAmount(), 3861, 'TotalCharge amount int this mock should be 3861');

        }
    }
}