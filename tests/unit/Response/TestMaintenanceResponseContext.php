<?php

use WPDesk\SaasPlatformClient\Response\RawResponse;
use WPDesk\SaasPlatformClient\Response\Maintenance\MaintenanceResponseContext;

class TestMaintenanceResponseContext extends \PHPUnit\Framework\TestCase
{
    public function testRawResponseMaintenance()
    {
        $rawRawResponse =
            '{"maintenance_till":1577833200,"message":"Please update your UPS plugin to the newest version."}';

        $rawResponse = new RawResponse(json_decode($rawRawResponse, true), 503, array());

        $maintenanceResponseContext = new MaintenanceResponseContext($rawResponse);

        $this->assertTrue($rawResponse->isMaintenance());

        $this->assertEquals(1577833200, $maintenanceResponseContext->getMaintenanceTill());

        $this->assertEquals(
            'Please update your UPS plugin to the newest version.',
            $maintenanceResponseContext->getMaintenanceMessage()
        );

    }

}