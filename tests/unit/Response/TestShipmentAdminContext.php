<?php

use WPDesk\SaasPlatformClient\Authentication\JWTToken;
use WPDesk\SaasPlatformClient\HttpClient\HttpClient;
use WPDesk\SaasPlatformClient\Model\Shipment\ShipmentAdminContext;
use WPDesk\SaasPlatformClient\PlatformFactory;
use WPDesk\SaasPlatformClient\PlatformFactoryOptions;

class TestShipmentAdminContext extends \PHPUnit\Framework\TestCase
{

    /** @var \WPDesk\SaasPlatformClient\PlatformAdminContext */
    private $platform;

    private function preparePlatform()
    {
        $options = new PlatformFactoryOptions();
        $this->platform = PlatformFactory::createPlatformApiForAdmin('', '', $options);
    }

    protected function setUp()
    {
        $this->preparePlatform();
        $tokenMock = Mockery::mock(JWTToken::class);
        $tokenMock->shouldReceive('isExpired')->andReturnFalse();
        $tokenMock->shouldReceive('isSignatureValid')->andReturnTrue();
        $tokenMock->shouldReceive('getAuthString')->andReturns('Bearer: whatever');


        /** @var JWTToken $tokenMock */
        $this->platform->setToken($tokenMock);
    }

    public function testList()
    {
        $rawListResponse = '{
    "_links": {
        "self": {
            "href": "/api/v1/shipments"
        },
        "item": [
            {
                "href": "/api/v1/shipments/1"
            }
        ]
    },
    "totalItems": 1,
    "itemsPerPage": 30,
    "_embedded": {
        "item": [
            {
                "_links": {
                    "self": {
                        "href": "/api/v1/shipments/1"
                    },
                    "shop": {
                        "href": "/api/v1/shops/1"
                    },
                    "shipping_service": {
                        "href": "/api/v1/shipping_services/1"
                    }
                },
                "_embedded": {
                    "shop": {
                        "_links": {
                            "self": {
                                "href": "/api/v1/shops/1"
                            }
                        },
                        "id": 1,
                        "domain": "wpdesk.org"
                    },
                    "shipping_service": {
                        "_links": {
                            "self": {
                                "href": "/api/v1/shipping_services/1"
                            }
                        },
                        "id": 1,
                        "name": "UPS",
                        "description": "desc",
                        "logo_url": "",
                        "enabled": true,
                        "connection_settings_definition_json": [
                            {
                                "title": "Credentials",
                                "fields": [
                                    {
                                        "id": "account_number",
                                        "name": "Account number",
                                        "type": "text",
                                        "desc": "",
                                        "required": true
                                    },
                                    {
                                        "id": "user_identifier",
                                        "name": "Username",
                                        "type": "text",
                                        "desc": "",
                                        "required": true
                                    },
                                    {
                                        "id": "password",
                                        "name": "Password",
                                        "type": "password",
                                        "desc": "",
                                        "required": true
                                    },
                                    {
                                        "id": "access_key",
                                        "name": "Api key",
                                        "type": "text",
                                        "desc": "",
                                        "required": true
                                    }
                                ]
                            },
                            {
                                "title": "Shipper Address",
                                "fields": [
                                    {
                                        "id": "name",
                                        "name": "Company name",
                                        "type": "text",
                                        "desc": "",
                                        "required": true
                                    },
                                    {
                                        "id": "address_line1",
                                        "name": "Origin address line",
                                        "type": "text",
                                        "desc": "",
                                        "required": true,
                                        "default-woocommerce": "woocommerce_store_address"
                                    },
                                    {
                                        "id": "city",
                                        "name": "Origin city",
                                        "type": "text",
                                        "desc": "",
                                        "required": true,
                                        "default-woocommerce": "woocommerce_store_city"
                                    },
                                    {
                                        "id": "postal_code",
                                        "name": "Origin postal code",
                                        "type": "text",
                                        "desc": "",
                                        "required": true,
                                        "default-woocommerce": "woocommerce_store_postcode"
                                    },
                                    {
                                        "id": "country_state_code",
                                        "name": "Origin country code",
                                        "type": "select_country_state",
                                        "desc": "",
                                        "required": true,
                                        "default-woocommerce": "woocommerce_default_country"
                                    },
                                    {
                                        "id": "bank_account",
                                        "name": "Bank account number",
                                        "type": "text",
                                        "desc": "",
                                        "required": false
                                    },
                                    {
                                        "id": "phone_number",
                                        "name": "Phone number",
                                        "type": "text",
                                        "required": false,
                                        "tooltip": {
                                            "title": "Phone number",
                                            "description": "If ShipTo country or territory is US, PR, CA, and VI, the layout is area code, 7 digit PhoneNumber or area code, 7 digit PhoneNumber, 4 digit extension number; number. For other countries or territories, the layout is CountryCode, area code, 7 digit number"
                                        }
                                    },
                                    {
                                        "id": "email",
                                        "name": "Email",
                                        "type": "text",
                                        "required": false
                                    }
                                ]
                            },
                            {
                                "title": "Shipment settings",
                                "fields": [
                                    {
                                        "id": "units",
                                        "name": "Weight/Dimension Units",
                                        "type": "select",
                                        "desc": "",
                                        "required": true,
                                        "default": "metric",
                                        "select-additional": {
                                            "options": [
                                                {
                                                    "text": "metric",
                                                    "value": "metric"
                                                },
                                                {
                                                    "text": "imperial",
                                                    "value": "imperial"
                                                }
                                            ]
                                        }
                                    }
                                ]
                            },
                            {
                                "title": "Label settings",
                                "fields": [
                                    {
                                        "id": "label_format",
                                        "name": "Label format",
                                        "type": "select",
                                        "desc": "",
                                        "required": true,
                                        "default": "pdf",
                                        "select-additional": {
                                            "options": [
                                                {
                                                    "text": "PDF",
                                                    "value": "pdf"
                                                },
                                                {
                                                    "text": "GIF",
                                                    "value": "gif"
                                                },
                                                {
                                                    "text": "ZPL",
                                                    "value": "zpl"
                                                },
                                                {
                                                    "text": "EPL",
                                                    "value": "epl"
                                                },
                                                {
                                                    "text": "SPL",
                                                    "value": "spl"
                                                }
                                            ]
                                        }
                                    },
                                    {
                                        "id": "label_size",
                                        "name": "Label size",
                                        "type": "select",
                                        "desc": "",
                                        "required": true,
                                        "default": "",
                                        "select-additional": {
                                            "options": [
                                                {
                                                    "text": "Not applicable",
                                                    "value": ""
                                                },
                                                {
                                                    "text": "4x6",
                                                    "value": "4x6"
                                                },
                                                {
                                                    "text": "4x8",
                                                    "value": "4x8"
                                                }
                                            ]
                                        }
                                    }
                                ]
                            },
                            {
                                "title": "Email notifications",
                                "fields": [
                                    {
                                        "id": "email_add_tracking_number",
                                        "name": "Add tracking link to customer emails",
                                        "type": "checkbox",
                                        "required": true,
                                        "default": "0",
                                        "tooltip": {
                                            "title": "Tracking number",
                                            "description": "Add tracking number and link to the WooCommerce confirmation email."
                                        },
                                        "additional-info": "warning: Id email_add_tracking_number is unique id for plugin to parse"
                                    }
                                ]
                            },
                            {
                                "title": "Advanced Options (API Status)",
                                "fields": [
                                    {
                                        "id": "connection_status",
                                        "name": "API Status",
                                        "type": "connection_status",
                                        "tooltip": {
                                            "title": "API Status",
                                            "description": "If there are connections problem,  you should see the error message."
                                        }
                                    }
                                ]
                            }
                        ],
                        "capabilities_json": [],
                        "connection_settings_values_json_schema": {
                            "type": "object",
                            "required": [
                                "account_number",
                                "user_identifier",
                                "password",
                                "access_key",
                                "name",
                                "address_line1",
                                "city",
                                "postal_code",
                                "country_code",
                                "bank_account",
                                "units",
                                "label_format",
                                "label_size",
                                "email_add_tracking_number"
                            ],
                            "properties": {
                                "account_number": {
                                    "type": "string",
                                    "title": "Account number",
                                    "minLength": 1
                                },
                                "user_identifier": {
                                    "type": "string",
                                    "title": "Username",
                                    "minLength": 1
                                },
                                "password": {
                                    "type": "string",
                                    "title": "Password",
                                    "minLength": 1
                                },
                                "access_key": {
                                    "type": "string",
                                    "title": "Key",
                                    "minLength": 1
                                },
                                "production": {
                                    "type": "boolean",
                                    "title": "Is production settings",
                                    "default": true
                                },
                                "address_line1": {
                                    "type": "string",
                                    "minLength": 1
                                },
                                "city": {
                                    "type": "string",
                                    "minLength": 1
                                },
                                "postal_code": {
                                    "type": "string",
                                    "minLength": 1
                                },
                                "country_code": {
                                    "type": "string",
                                    "minLength": 1
                                },
                                "country_state": {
                                    "type": "string"
                                },
                                "bank_account": {
                                    "type": "string",
                                    "minLength": 1
                                },
                                "units": {
                                    "type": "string",
                                    "minLength": 1
                                },
                                "label_format": {
                                    "type": "string",
                                    "minLength": 1
                                },
                                "label_size": {
                                    "type": "string",
                                    "minLength": 0
                                },
                                "email_add_tracking_number": {
                                    "type": "boolean"
                                },
                                "phone_number": {
                                    "type": "string"
                                },
                                "email": {
                                    "type": "string"
                                }
                            }
                        }
                    }
                },
                "id": 1,
                "tracking_numbers": [
                    "1ZA3E9726893592032"
                ],
                "tracking_url": "https://www.ups.com/track?loc=en_GB&tracknum=1ZA3E9726893592032/trackdetails",
                "status": "confirmed",
                "created_at": "2018-09-25T14:32:55+00:00",
                "tracking_numbers_as_string": "1ZA3E9726893592032"
            }
        ]
    }
}';

        $httpClientMock = Mockery::mock(HttpClient::class);
        $httpClientMock
            ->shouldReceive('send')
            ->withAnyArgs()
            ->andReturn(new WPDesk\SaasPlatformClient\HttpClient\HttpClientResponse('[]',
                $rawListResponse, 200));
        /** @var HttpClient $httpClientMock */
        $this->platform->getClient()->setHttpClient($httpClientMock);;

        $user_id = '1234';

        $shipmentsProvidersResponse = $this->platform->requestGetListShipments($user_id);
        foreach ($shipmentsProvidersResponse->getPage() as $provider) {
            $this->assertInstanceOf(ShipmentAdminContext::class, $provider, 'List should be hydrated');

            /** @var ShipmentAdminContext $provider */
            $this->assertEquals('wpdesk.org', $provider->getShop()->getDomain());
            $this->assertEquals('confirmed', $provider->getStatus());
            $this->assertEquals('1', $provider->getId());
            $this->assertEquals('1ZA3E9726893592032', $provider->getTrackingNumbersAsString());
            $this->assertEquals('2018-09-25T14:32:55+00:00', $provider->getCreatedAt()->format('c'));
            $this->assertEquals('UPS', $provider->getShippingService()->getName());
        }

    }
}