<?php

use WPDesk\SaasPlatformClient\Authentication\JWTToken;
use WPDesk\SaasPlatformClient\HttpClient\HttpClient;
use WPDesk\SaasPlatformClient\Model\ShippingService;
use WPDesk\SaasPlatformClient\PlatformFactory;
use WPDesk\SaasPlatformClient\PlatformFactoryOptions;

class TestShippingServices extends \PHPUnit\Framework\TestCase
{

    /** @var \WPDesk\SaasPlatformClient\Platform */
    private $platform;

    private function preparePlatform()
    {
        $options = new PlatformFactoryOptions();
        $this->platform = PlatformFactory::createPlatformApi($options);
    }

    protected function setUp()
    {
        $this->preparePlatform();
        $tokenMock = Mockery::mock(JWTToken::class);
        $tokenMock->shouldReceive('isExpired')->andReturnFalse();
        $tokenMock->shouldReceive('isSignatureValid')->andReturnTrue();
        $tokenMock->shouldReceive('getAuthString')->andReturns('Bearer: whatever');


        /** @var JWTToken $tokenMock */
        $this->platform->setToken($tokenMock);
    }

    public function testList()
    {
        $totalItems = 2;
        $itemsPerPage = 30;

        $rawListResponse = '{
            "_embedded": {
                "item": [
                    {
                        "id": 1,
                        "name": "UPS",
                        "description": "UPS description"
                    },
                    {
                        "id": 2,
                        "name": "Meh",
                        "description": "asdfasdf"
                    }
                ]
            },
            "totalItems": ' . $totalItems . ',
            "itemsPerPage": ' . $itemsPerPage . '
        }';

        $httpClientMock = Mockery::mock(HttpClient::class);
        $httpClientMock
            ->shouldReceive('send')
            ->withAnyArgs()
            ->andReturn(new WPDesk\SaasPlatformClient\HttpClient\HttpClientResponse('[]',
                $rawListResponse, 200));
        /** @var HttpClient $httpClientMock */
        $this->platform->getClient()->setHttpClient($httpClientMock);;

        $shippingProvidersResponse = $this->platform->requestListShippingServices();
        foreach ($shippingProvidersResponse->getPage() as $provider) {
            $this->assertInstanceOf(ShippingService::class, $provider, 'List should be hydrated');
        }
        $this->assertEquals($itemsPerPage, $shippingProvidersResponse->getItemsPerPage(), 'Invalid item count per page');
        $this->assertEquals($totalItems, $shippingProvidersResponse->getItemCount(), 'Invalid item count');
    }
}