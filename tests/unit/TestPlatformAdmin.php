<?php

use WPDesk\SaasPlatformClient\HttpClient\HttpClient;
use WPDesk\SaasPlatformClient\HttpClient\HttpClientResponse;
use WPDesk\SaasPlatformClient\PlatformFactory;
use WPDesk\SaasPlatformClient\PlatformFactoryOptions;

class TestPlatformAdmin extends \PHPUnit\Framework\TestCase
{
    const SAAS_USER_ID = 7;
    const SAAS_PLAN_ID = 7;

    /** @var  string */
    private $apiKey;

    /** @var string */
    private $locale;

    /** @var  \WPDesk\SaasPlatformClient\PlatformAdminContext */
    private $platform;

    protected function setUp()
    {
        $options = new PlatformFactoryOptions();

        $this->apiKey = 'MTo1YmIzNTdjZGIzNGJlMi4zNDI2ODM0NA==';
        $this->locale = 'en_GB';

        $this->platform = PlatformFactory::createPlatformApiForAdmin($this->apiKey, $this->locale, $options);
    }

    public function testSaveSettingsSuccess()
    {
        $validTokenResponse = json_encode([
            TestPlatform::TOKEN_FIELD => 'whatever'
        ], JSON_FORCE_OBJECT);

        $validUserResponse = '{
            "_links": {
                "self": {
                    "href": "/api/v1/users/'. self::SAAS_USER_ID .'"
                },
                "shipping_plan": {
                    "href": "/api/v1/shipping_plans/'. self::SAAS_PLAN_ID .'"
                },
                "shops": [
                    {
                        "href": "/api/v1/shops/1"
                    }
                ]
            },
            "_embedded": {
                "shipping_plan": {
                    "_links": {
                        "self": {
                            "href": "/api/v1/shipping_plans/'. self::SAAS_PLAN_ID .'"
                        }
                    },
                    "id": '. self::SAAS_PLAN_ID .',
                    "package_limit": 50,
                    "user_count_packages_in_period": 0,
                    "period_start": "2018-10-02T12:32:52+00:00",
                    "period_end": "2018-11-02T12:32:52+00:00",
                    "user_count_package_in_period_left": 50
                },
                "shops": [
                    {
                        "_links": {
                            "self": {
                                "href": "/api/v1/shops/1"
                            }
                        },
                        "id": 1,
                        "domain": "wpdesk.org"
                    }
                ]
            },
            "id": ' . self::SAAS_USER_ID .',
            "email": "test2@wpdesk.org",
            "password": "Mzp3aGF0ZXZlcg==",
            "roles": [
                "ROLE_USER",
                "ROLE_SHOP"
            ]
        }';

        $httpClientMock = Mockery::mock(HttpClient::class);
        $httpClientMock
            ->shouldReceive('send')
            ->andReturnUsing(function ($url) use ($validTokenResponse, $validUserResponse) {
                static $count = 0;
                $count++;

                if (strpos($url, 'token') !== false) {
                    return new HttpClientResponse('[]', $validTokenResponse, TestPlatform::REQUEST_SUCCESS_CODE);
                }

                if (strpos($url, 'users') !== false) {
                    return new HttpClientResponse('[]', $validUserResponse, TestPlatform::REQUEST_SUCCESS_CODE);
                }

                return new HttpClientResponse('[]', '[]', TestPlatform::REQUEST_SUCCESS_CODE);
            });

        /** @var HttpClient $httpClientMock */
        $this->platform->getClient()->setHttpClient($httpClientMock);

        $plan = new \WPDesk\SaasPlatformClient\Model\ShippingPlan();
        $plan->setPeriodStart(new DateTime());
        $plan->setPeriodEnd(new DateTime('+1 month'));
        $plan->setPackageLimit(75);

        $response = $this->platform->requestSavePlan($plan, self::SAAS_USER_ID);

        $this->assertTrue(!$response->isError(), 'Plan should be correctly saved');
    }
}