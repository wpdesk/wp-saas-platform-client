<?php

use WPDesk\SaasPlatformClient\Authentication\Token;
use WPDesk\SaasPlatformClient\Request\ShippingServices\GetServiceRequest;
use WPDesk\SaasPlatformClient\Cache\HowToCache;
use WPDesk\SaasPlatformClient\Request\ShippingServicesSettings\PutSettingsRequest;
use WPDesk\SaasPlatformClient\Model\ShippingServiceSetting;

class TestRequestCacheInfoResolver extends \WP_Mock\Tools\TestCase
{

    /** @var \WPDesk\SaasPlatformClient\Cache\CacheInfoResolver */
    private $cacheInfoResolver;

    public function setUp()
    {
        $this->cacheInfoResolver = new \WPDesk\SaasPlatformClient\ApiClient\RequestCacheInfoResolver();
        \WP_Mock::setUp();
    }

    public function tearDown()
    {
        \WP_Mock::tearDown();
    }

    public function testIsSupported()
    {
        $token = $this->createMock(Token::class);

        $request = new GetServiceRequest($token, 1);

        $this->assertTrue($this->cacheInfoResolver->isSupported($request), 'GetServiceRequest should be supported.');

        $this->assertFalse($this->cacheInfoResolver->isSupported(new stdClass()), 'stdClass is not supported.');
    }

    public function testShouldCache()
    {
        $token = $this->createMock(Token::class);

        $request = new GetServiceRequest($token, 1);

        $this->assertTrue($this->cacheInfoResolver->shouldCache($request), 'GetServiceRequest should be cached.');

        $this->assertFalse($this->cacheInfoResolver->shouldCache(new stdClass()), 'stdClass should not be supported.');

        $tokenRequest = new \WPDesk\SaasPlatformClient\Request\Authentication\TokenRequest('', '', '');

        $this->assertFalse(
            $this->cacheInfoResolver->shouldCache($tokenRequest),
            'POST requests should not be supported.'
        );
    }

    public function testPrepareHowToCache()
    {
        $token = $this->createMock(Token::class);

        $request = new GetServiceRequest($token, 1);

        $this->assertInstanceOf(
            HowToCache::class,
            $this->cacheInfoResolver->prepareHowToCache($request)
        );
    }

    public function testShouldClearCache()
    {

        $token = $this->createMock(Token::class);

        $request = new GetServiceRequest($token, 1);

        $jsonValue = '{
                "account_number": "x",
                "username": "ya",
                "password": "z",
                "key": "aa"
            }';

        $rawResponse = new \WPDesk\SaasPlatformClient\Response\RawResponse(
            json_decode($jsonValue, true),
            200,
            array( \WPDesk\SaasPlatformClient\Response\RawResponse::HEADER_X_PLATFORM_VERSION_HASH => 'A' )
        );

        $item = new \WPDesk\SaasPlatformClient\Response\ShippingServices\GetShippingServiceResponse($rawResponse);

        \WP_Mock::userFunction(
            'get_option',
            array(
                'times'  => 1,
                'return' => 'A',
            )
        );

        $this->assertFalse(
            $this->cacheInfoResolver->shouldClearCache($request, $item),
            'GetServiceRequest should not clear cache.'
        );

        $shippingServiceSetting = $this->createMock(
            \WPDesk\SaasPlatformClient\Model\ShippingServiceSetting::class
        );

        $request = new PutSettingsRequest($token, $shippingServiceSetting);

        $this->assertTrue(
            $this->cacheInfoResolver->shouldClearCache($request, $item),
            'PutSettingsRequest should clear cache.'
        );
    }

    public function testShouldClearCacheWithPlatformVersion()
    {

        $token = $this->createMock(Token::class);

        $request = new GetServiceRequest($token, 1);

        $jsonValue = '{
                "account_number": "x",
                "username": "ya",
                "password": "z",
                "key": "aa"
            }';

        $rawResponse = new \WPDesk\SaasPlatformClient\Response\RawResponse(
            json_decode($jsonValue, true),
            200,
            array(\WPDesk\SaasPlatformClient\Response\RawResponse::HEADER_X_PLATFORM_VERSION_HASH => 'A')
        );

        \WP_Mock::userFunction(
            'get_option',
            array(
                'times'  => 1,
                'return' => 'A',
            )
        );

        $item = new \WPDesk\SaasPlatformClient\Response\ShippingServices\GetShippingServiceResponse($rawResponse);

        $this->assertFalse(
            $this->cacheInfoResolver->shouldClearCache($request, $item),
            'Should be cached - platform version is same as stored!'
        );

        \WP_Mock::userFunction(
            'get_option',
            array(
                'times'  => 1,
                'return' => 'B',
            )
        );

        \WP_Mock::userFunction(
            'update_option',
            array(
                'times'  => 1,
                'return' => true,
            )
        );

        $item = new \WPDesk\SaasPlatformClient\Response\ShippingServices\GetShippingServiceResponse($rawResponse);

        $this->assertTrue(
            $this->cacheInfoResolver->shouldClearCache($request, $item),
            'Should not be cached - platform version is not same as stored!'
        );
    }

    public function testShouldClearKeys()
    {
        $token = $this->createMock(Token::class);

        $request = new GetServiceRequest($token, 1);

        $jsonValue = '{
                "account_number": "x",
                "username": "ya",
                "password": "z",
                "key": "aa"
            }';

        $rawResponse = new \WPDesk\SaasPlatformClient\Response\RawResponse(
            json_decode($jsonValue, true),
            200,
            array( \WPDesk\SaasPlatformClient\Response\RawResponse::HEADER_X_PLATFORM_VERSION_HASH => 'A' )
        );

        $item = new \WPDesk\SaasPlatformClient\Response\ShippingServices\GetShippingServiceResponse($rawResponse);

        \WP_Mock::userFunction(
            'get_option',
            array(
                'times'  => 1,
                'return' => 'A',
            )
        );

        $this->assertFalse(
            $this->cacheInfoResolver->shouldClearCache($request, $item),
            'GetServiceRequest should not clear cache.'
        );

        $shippingServiceSetting = new ShippingServiceSetting(array('shipping_service' => '1', 'shop' => '1'));

        $request = new PutSettingsRequest($token, $shippingServiceSetting);

        $this->assertEquals(
            array(md5($request->getEndpoint())),
            $this->cacheInfoResolver->shouldClearKeys($request, $item)
        );
    }

}
