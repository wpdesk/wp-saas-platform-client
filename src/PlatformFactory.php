<?php

namespace WPDesk\SaasPlatformClient;

use WPDesk\SaasPlatformClient\ApiClient\ClientFactory;
use WPDesk\SaasPlatformClient\Persistence\PersistentContainerFactory;

class PlatformFactory
{
    /**
     * Creates Flexible Connect Platform instance
     *
     * @param PlatformFactoryOptions $options
     * @return Platform
     */
    public static function createPlatformApi(PlatformFactoryOptions $options)
    {
        $clientFactory = new ClientFactory();
        $persistenceFactory = new PersistentContainerFactory();
        return new Platform(
            $clientFactory->createClient($options),
            $persistenceFactory->createContainer($options),
            $options->getLogger());
    }

    /**
     * Creates Flexible Connect Platform instance in administration context
     *
     * @param string $adminConnectKey Connect key for admin account
     * @param string $locale Locale in format xx_XX
     * @param PlatformFactoryOptions $options
     *
     * @return PlatformAdminContext
     */
    public static function createPlatformApiForAdmin($adminConnectKey, $locale, PlatformFactoryOptions $options)
    {
        $clientFactory = new ClientFactory();
        $persistenceFactory = new PersistentContainerFactory();
        $persistanceContainer = $persistenceFactory->createContainer($options);

        $persistanceContainer->set(Platform::CONTAINER_KEY_CONNECT_KEY, $adminConnectKey);
        $persistanceContainer->set(Platform::CONTAINER_KEY_DOMAIN, 'wpdesk.org');
        $persistanceContainer->set(Platform::CONTAINER_KEY_LOCALE, $locale);

        return new PlatformAdminContext(
            $clientFactory->createClient($options),
            $persistanceContainer,
            $options->getLogger()
        );
    }
}