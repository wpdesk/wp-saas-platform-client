<?php

namespace WPDesk\SaasPlatformClient\Serializer;

use WPDesk\SaasPlatformClient\PlatformOption\SerializerOptions;

class SerializerFactory
{
    /**
     * @param SerializerOptions $options
     * @return Serializer
     */
    public function createSerializer(SerializerOptions $options)
    {
        $className = $options->getSerializerClass();
        return new $className;
    }
}