<?php

namespace WPDesk\SaasPlatformClient\Serializer\Exception;

/**
 * Thrown when serializer cannot unserialize string data
 *
 * @package WPDesk\SaasPlatformClient\Serializer\Exception
 */
class CannotUnserializeException extends \RuntimeException
{

}