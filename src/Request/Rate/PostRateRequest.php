<?php

namespace WPDesk\SaasPlatformClient\Request\Rate;

use WPDesk\SaasPlatformClient\Authentication\Token;
use WPDesk\SaasPlatformClient\Model\Rate\RateRequest;
use WPDesk\SaasPlatformClient\Request\AuthRequest;

final class PostRateRequest extends AuthRequest
{
    /** @var string */
    protected $method = 'POST';

    /** @var string */
    protected $endPoint = '/shops/{shop}/shipping_services/{service}/rates';

    /**
     * PostRateRequest constructor.
     * @param Token $token
     * @param int $shippingServiceId
     * @param int $shopId
     * @param RateRequest $request
     */
    public function __construct(
        Token $token,
        $shippingServiceId,
        $shopId,
        RateRequest $request
    ) {
        parent::__construct($token);

        $this->endPoint = str_replace(['{shop}', '{service}'], [$shopId, $shippingServiceId], $this->endPoint);

        $this->data = $request->toArray();
    }
}