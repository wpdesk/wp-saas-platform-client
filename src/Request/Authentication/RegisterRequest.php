<?php

namespace WPDesk\SaasPlatformClient\Request\Authentication;

use WPDesk\SaasPlatformClient\Request\BasicRequest;

final class RegisterRequest extends BasicRequest
{
    /** @var string */
    protected $method = 'POST';

    /** @var string */
    protected $endPoint = '/register';

    /**
     * RegisterRequest constructor.
     * @param string $email
     * @param string $domain
     * @param string $locale
     * @param ?string $admin_url
     */
    public function __construct($email, $domain, $locale, $admin_url = null)
    {
        $this->data = compact('email', 'domain', 'locale');
        if ($admin_url !== null) {
            $this->data['adminUrl'] = $admin_url;
        }
    }
}