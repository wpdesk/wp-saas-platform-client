<?php

namespace WPDesk\SaasPlatformClient\Request\Authentication;


use WPDesk\SaasPlatformClient\Request\BasicRequest;

final class TokenRequest extends BasicRequest
{
    /** @var string */
    protected $method = 'POST';

    /** @var string */
    protected $endPoint = '/token';

    /**
     * TokenRequest constructor.
     * @param string $connectKey
     * @param string $domain
     * @param string $locale
     */
    public function __construct($connectKey, $domain, $locale)
    {
        $this->data = compact(['connectKey', 'domain', 'locale']);
    }
}