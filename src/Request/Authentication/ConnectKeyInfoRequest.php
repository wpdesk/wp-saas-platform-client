<?php

namespace WPDesk\SaasPlatformClient\Request\Authentication;

use WPDesk\SaasPlatformClient\Authentication\Token;
use WPDesk\SaasPlatformClient\Request\AuthRequest;

final class ConnectKeyInfoRequest extends AuthRequest
{
    /** @var string */
    protected $method = 'GET';

    /** @var string */
    protected $endPoint = '/connectkeyinfo/{connectKey}';


    /**
     * ConnectKeyInfoRequest constructor.
     * @param $connectKey
     * @param Token $token
     */
    public function __construct($connectKey, Token $token)
    {
        parent::__construct($token);

        $this->endPoint = str_replace('{connectKey}', $connectKey, $this->endPoint);
    }
}