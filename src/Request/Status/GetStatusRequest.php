<?php

namespace WPDesk\SaasPlatformClient\Request\Status;

use WPDesk\SaasPlatformClient\Authentication\Token;
use WPDesk\SaasPlatformClient\Request\AuthRequest;
use WPDesk\SaasPlatformClient\Request\BasicRequest;

final class GetStatusRequest extends BasicRequest
{

    /** @var string */
    protected $method = 'GET';

    /** @var string */
    protected $endPoint = '/status';

    /** @var Token */
    private $token;

    /**
     * GetStatusRequest constructor.
     *
     * @param null|Token $token
     */
    public function __construct(
        Token $token = null
    ) {
        $this->token = $token;
    }

    /**
     * Returns array of http headers
     *
     * @return array
     */
    public function getHeaders()
    {
        if (null !== $this->token) {
            $headers = array(
                'Authorization' => $this->token->getAuthString()
            );
            return array_merge($headers, parent::getHeaders());
        } else {
            return parent::getHeaders();
        }
    }

}