<?php

namespace WPDesk\SaasPlatformClient\Request;

use WPDesk\SaasPlatformClient\Authentication\Token;

class AuthRequest extends BasicRequest
{
    /** @var Token */
    private $token;

    public function __construct(Token $token)
    {
        $this->setToken($token);
    }

    /**
     * @param Token $token
     */
    public function setToken(Token $token)
    {
        $this->token = $token;
    }

    /**
     * @return bool
     */
    public function isTokenExpired()
    {
        return $this->token->isExpired();
    }

    /**
     * @return bool
     */
    public function isTokenInvalid()
    {
        return !$this->token->isSignatureValid();
    }

    /**
     * Returns array of http headers
     *
     * @return array
     */
    public function getHeaders()
    {
        $headers = array(
            'Authorization' => $this->token->getAuthString()
        );
        return array_merge($headers, parent::getHeaders());
    }
}