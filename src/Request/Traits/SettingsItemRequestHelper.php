<?php

namespace WPDesk\SaasPlatformClient\Request\Traits;

trait SettingsItemRequestHelper
{

    /**
     * Build multiple compound key id in api-platform
     *
     * @param int $shippingServiceId
     * @param int $shopId
     * @param string|null $settingType
     * @return string
     */
    private function buildSettingsId($shippingServiceId, $shopId, $settingType = null)
    {
        $query_data = [
            'shippingService' => $shippingServiceId,
            'shop' => $shopId,
            'type' => $settingType
        ];
        if (null !== $settingType) {
            $query_data['type'] = $settingType;
        }
        return http_build_query($query_data, null, ';');
    }
}