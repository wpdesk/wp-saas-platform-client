<?php

namespace WPDesk\SaasPlatformClient\Request\Shipment;

use WPDesk\SaasPlatformClient\Authentication\Token;
use WPDesk\SaasPlatformClient\Model\Shipment\ShipmentRequest;
use WPDesk\SaasPlatformClient\Request\AuthRequest;

final class GetUserListShipmentRequest extends AuthRequest
{
    /** @var string */
    protected $method = 'GET';

    /** @var string */
    protected $endPoint = '/users/{id}/shipments';

    /**
     * GetListShipmentRequest constructor.
     * @param Token $token
     */
    public function __construct($user_id, Token $token)
{
        parent::__construct($token);

        $this->endPoint = str_replace('{id}', $user_id, $this->endPoint);
    }
}