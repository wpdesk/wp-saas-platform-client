<?php

namespace WPDesk\SaasPlatformClient\Request\ShippingPlan;

use WPDesk\SaasPlatformClient\Authentication\Token;
use WPDesk\SaasPlatformClient\Model\ShippingPlan;
use WPDesk\SaasPlatformClient\Request\AuthRequest;


final class PutShippingPlanRequest extends AuthRequest
{
    /** @var string */
    protected $method = 'PUT';

    /** @var ShippingPlan */
    private $plan;

    /** @var string */
    protected $endPoint = '/shipping_plans/{id}';

    public function __construct(Token $token, ShippingPlan $plan)
    {
        parent::__construct($token);
        $this->plan = $plan;

        $this->data = $plan->toArray();

        $this->endPoint = str_replace('{id}', $plan->getId(), $this->endPoint);
    }
}