<?php

namespace WPDesk\SaasPlatformClient\Request\Fields;

use WPDesk\SaasPlatformClient\Authentication\Token;
use WPDesk\SaasPlatformClient\Request\AuthRequest;

final class GetFieldsRequest extends AuthRequest
{
    /** @var string */
    protected $method = 'GET';

    /** @var string */
    protected $endPoint = '/shops/{shop}/shipping_services/{service}/dynamic_fields/{zone_targets}';

    const ZONE_TARGETS_DELIMITER = ',';

    /**
     * @param array $zoneTargets
     * @return string
     */
    private function serializeZoneTargets(array $zoneTargets)
    {
        return implode(self::ZONE_TARGETS_DELIMITER, $zoneTargets);
    }

    /**
     * PostShipmentRequest constructor.
     * @param Token $token
     * @param int $shippingServiceId
     * @param int $shopId
     * @param string[] $zoneTargets
     */
    public function __construct(
        Token $token,
        $shippingServiceId,
        $shopId,
        array $zoneTargets
    ) {
        parent::__construct($token);

        $this->endPoint = str_replace(['{shop}', '{service}', '{zone_targets}'],
            [$shopId, $shippingServiceId, $this->serializeZoneTargets($zoneTargets)], $this->endPoint);
    }
}