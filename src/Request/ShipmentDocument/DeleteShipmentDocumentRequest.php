<?php

namespace WPDesk\SaasPlatformClient\Request\ShipmentDocument;

use WPDesk\SaasPlatformClient\Authentication\Token;
use WPDesk\SaasPlatformClient\Request\AuthRequest;

final class DeleteShipmentDocumentRequest extends AuthRequest
{

    /** @var string */
    protected $method = 'DELETE';

    /** @var string */
    protected $endPoint = '/documents/{document}';

    /**
     * PostShipmentRequest constructor.
     * @param Token $token
     * @param int $documentId
     */
    public function __construct(
        Token $token,
        $documentId
    ) {
        parent::__construct($token);

        $this->endPoint = str_replace(
            '{document}',
            $documentId,
            $this->endPoint
        );
    }

}