<?php

namespace WPDesk\SaasPlatformClient\Request\ParcelCollectionPoints;

use WPDesk\SaasPlatformClient\Authentication\Token;
use WPDesk\SaasPlatformClient\Request\AuthRequest;

final class GetSingleRequest extends AuthRequest
{
    /** @var string */
    protected $method = 'GET';

    /** @var string */
    protected $endPoint = '/shops/{shop}/shipping_services/{service}/ap/{id}';

    const ZONE_TARGETS_DELIMITER = ',';


    public function __construct(
        Token $token,
        $shippingServiceId,
        $shopId,
        $collectionPointId
    ) {
        parent::__construct($token);

        $this->endPoint = str_replace(
            ['{shop}', '{service}', '{id}'],
            [$shopId, $shippingServiceId, $collectionPointId],
            $this->endPoint
        );
    }
}