<?php

namespace WPDesk\SaasPlatformClient\Request\ParcelCollectionPoints;

use WPDesk\SaasPlatformClient\Model\ParcelCollectionPoints\SearchRequest;
use WPDesk\SaasPlatformClient\Authentication\Token;
use WPDesk\SaasPlatformClient\Request\AuthRequest;

final class PostSearchRequest extends AuthRequest
{

    /** @var string */
    protected $method = 'POST';

    /** @var string */
    protected $endPoint = '/shops/{shop}/shipping_services/{service}/ap';

    /**
     * PostRateRequest constructor.
     * @param Token $token
     * @param int $shippingServiceId
     * @param int $shopId
     * @param SearchRequest $request
     */
    public function __construct(
        Token $token,
        $shippingServiceId,
        $shopId,
        SearchRequest $request
    ) {
        parent::__construct($token);

        $this->endPoint = str_replace(['{shop}', '{service}'], [$shopId, $shippingServiceId], $this->endPoint);

        $this->data = $request->toArray();
    }

}