<?php

namespace WPDesk\SaasPlatformClient\Request\ShippingServicesSettings;

use WPDesk\SaasPlatformClient\Authentication\Token;
use WPDesk\SaasPlatformClient\Model\ShippingServiceSetting;
use WPDesk\SaasPlatformClient\Request\AuthRequest;
use WPDesk\SaasPlatformClient\Request\Traits\SettingsItemRequestHelper;

final class PostSettingsRequest extends AuthRequest
{
    use SettingsItemRequestHelper;

    /** @var string */
    protected $method = 'POST';

    /** @var ShippingServiceSetting */
    private $setting;

    /** @var string */
    protected $endPoint = '/shipping_services/settings';

    public function __construct(Token $token, ShippingServiceSetting $setting)
    {
        parent::__construct($token);
        $this->setting = $setting;

        $this->data = $setting->toArray();
    }
}