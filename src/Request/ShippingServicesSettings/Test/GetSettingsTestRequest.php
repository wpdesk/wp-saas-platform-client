<?php

namespace WPDesk\SaasPlatformClient\Request\ShippingServicesSettings\Test;

use WPDesk\SaasPlatformClient\Authentication\Token;
use WPDesk\SaasPlatformClient\Model\ShippingServiceSetting;
use WPDesk\SaasPlatformClient\Request\AuthRequest;

final class GetSettingsTestRequest extends AuthRequest
{
    /** @var string */
    protected $method = 'GET';

    /** @var string */
    protected $endPoint = '/shops/{shop}/shipping_services/{service}/settings/{type}/test';

    /**
     * PutSettingsRequest constructor.
     * @param Token $token
     * @param int $shippingServiceId
     * @param int $shopId
     */
    public function __construct(
        Token $token,
        $shippingServiceId,
        $shopId
    ) {
        parent::__construct($token);

        $this->endPoint = str_replace(
            ['{shop}', '{service}', '{type}'],
            [$shopId, $shippingServiceId, ShippingServiceSetting::TYPE_CONNECTION_SETTINGS],
            $this->endPoint
        );
    }

}