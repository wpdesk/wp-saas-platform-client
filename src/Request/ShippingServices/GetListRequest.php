<?php

namespace WPDesk\SaasPlatformClient\Request\ShippingServices;

use WPDesk\SaasPlatformClient\Request\AuthRequest;

final class GetListRequest extends AuthRequest
{
    /** @var string */
    protected $method = 'GET';

    /** @var string */
    protected $endPoint = '/shipping_services';
}