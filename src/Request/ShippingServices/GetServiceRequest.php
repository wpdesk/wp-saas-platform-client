<?php

namespace WPDesk\SaasPlatformClient\Request\ShippingServices;

use WPDesk\SaasPlatformClient\Authentication\Token;
use WPDesk\SaasPlatformClient\Request\AuthRequest;

final class GetServiceRequest extends AuthRequest
{
    /** @var string */
    protected $method = 'GET';

    /** @var string */
    protected $endPoint = '/shipping_services/{id}';

    /**
     * PutSettingsRequest constructor.
     * @param Token $token
     * @param int $shippingServiceId
     */
    public function __construct(
        Token $token,
        $shippingServiceId
    ) {
        parent::__construct($token);

        $this->endPoint = str_replace('{id}', $shippingServiceId, $this->endPoint);
    }
}