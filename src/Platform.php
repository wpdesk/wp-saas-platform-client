<?php

namespace WPDesk\SaasPlatformClient;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use WPDesk\Persistence\ElementNotExistsException;
use WPDesk\Persistence\PersistentContainer;
use WPDesk\SaasPlatformClient\Authentication\JWTSaasToken;
use WPDesk\SaasPlatformClient\Authentication\NullToken;
use WPDesk\SaasPlatformClient\Authentication\Token;
use WPDesk\SaasPlatformClient\ApiClient\Client;
use WPDesk\SaasPlatformClient\Model\ParcelCollectionPoints\SearchRequest;
use WPDesk\SaasPlatformClient\Model\ParcelCollectionPoints\SingleRequest;
use WPDesk\SaasPlatformClient\Model\Rate\RateRequest;
use WPDesk\SaasPlatformClient\Model\Shipment\ShipmentRequest;
use WPDesk\SaasPlatformClient\Model\ShipmentDocument\ShipmentDocumentRequest;
use WPDesk\SaasPlatformClient\Model\ShippingServiceSetting;
use WPDesk\SaasPlatformClient\Request\Authentication\RegisterRequest;
use WPDesk\SaasPlatformClient\Request\AuthRequest;
use WPDesk\SaasPlatformClient\Request\ShipmentCancel\PostCancelRequest;
use WPDesk\SaasPlatformClient\Request\Fields\GetFieldsRequest;
use WPDesk\SaasPlatformClient\Request\Label\PostLabelRequest;
use WPDesk\SaasPlatformClient\Request\ParcelCollectionPoints\GetSingleRequest;
use WPDesk\SaasPlatformClient\Request\ParcelCollectionPoints\PostSearchRequest;
use WPDesk\SaasPlatformClient\Request\Rate\PostRateRequest;
use WPDesk\SaasPlatformClient\Request\ShipmentDocument\DeleteShipmentDocumentRequest;
use WPDesk\SaasPlatformClient\Request\ShipmentDocument\PostShipmentDocumentRequest;
use WPDesk\SaasPlatformClient\Request\ShipmentDocumentsSend\PostSendRequest;
use WPDesk\SaasPlatformClient\Request\ShippingServices\GetListRequest;
use WPDesk\SaasPlatformClient\Request\ShippingServices\GetServiceRequest;
use WPDesk\SaasPlatformClient\Request\Shipment\PostShipmentRequest;
use WPDesk\SaasPlatformClient\Request\ShippingServicesSettings\GetSettingsRequest;
use WPDesk\SaasPlatformClient\Request\ShippingServicesSettings\PostSettingsRequest;
use WPDesk\SaasPlatformClient\Request\ShippingServicesSettings\PutSettingsRequest;
use WPDesk\SaasPlatformClient\Request\ShippingServicesSettings\Test\GetSettingsTestRequest;
use WPDesk\SaasPlatformClient\Request\Status\GetStatusRequest;
use WPDesk\SaasPlatformClient\Request\Users\GetUserRequest;
use WPDesk\SaasPlatformClient\Response\AuthApiResponse;
use WPDesk\SaasPlatformClient\Response\Authentication\RegisterResponse;
use WPDesk\SaasPlatformClient\Request\Authentication\TokenRequest;
use WPDesk\SaasPlatformClient\Response\Fields\GetFieldsResponse;
use WPDesk\SaasPlatformClient\Response\Label\PostLabelResponse;
use WPDesk\SaasPlatformClient\Response\ParcelCollectionPoints\GetSingleResponse;
use WPDesk\SaasPlatformClient\Response\ParcelCollectionPoints\PostSearchResponse;
use WPDesk\SaasPlatformClient\Response\ProtectedResponse;
use WPDesk\SaasPlatformClient\Response\Response;
use WPDesk\SaasPlatformClient\Response\ShipmentCancel\PostCancelResponse;
use WPDesk\SaasPlatformClient\Response\Rate\PostRateResponse;
use WPDesk\SaasPlatformClient\Response\ShipmentDocument\DeleteShipmentDocumentResponse;
use WPDesk\SaasPlatformClient\Response\ShipmentDocument\PostShipmentDocumentResponse;
use WPDesk\SaasPlatformClient\Response\ShipmentDocumentsSend\PostSendResponse;
use WPDesk\SaasPlatformClient\Response\ShippingServices;
use WPDesk\SaasPlatformClient\Response\Authentication\TokenResponse;
use WPDesk\SaasPlatformClient\Response\ShippingServicesSettings\GetSettingsResponse;
use WPDesk\SaasPlatformClient\Response\Shipment\PostShipmentResponse;
use WPDesk\SaasPlatformClient\Response\ShippingServicesSettings\Test\GetSettingsTestResponse;
use WPDesk\SaasPlatformClient\Response\Status\GetStatusResponse;
use WPDesk\SaasPlatformClient\Response\Users\GetUserResponse;

class Platform implements LoggerAwareInterface
{
    const CONTAINER_KEY_TOKEN = 'token';
    const CONTAINER_KEY_CONNECT_KEY = 'connectKey';
    const CONTAINER_KEY_DOMAIN = 'domain';
    const CONTAINER_KEY_LOCALE = 'locale';

    const LIBARY_LOGIN_CONTEXT = 'wp-saas-platform-client';

    /** @var Client */
    private $client;

    /** @var  PersistentContainer */
    private $persistentContainer;

    /** @var LoggerInterface */
    private $logger;

    /**
     * Platform constructor.
     * @param Client $client
     * @param PersistentContainer $container
     * @param LoggerInterface $logger
     */
    public function __construct(Client $client, PersistentContainer $container, LoggerInterface $logger)
    {
        $this->persistentContainer = $container;
        $this->client = $client;
        $this->logger = $logger;
        $this->logger->debug('Platform instance created', $this->getLoggerContext());
    }

    /**
     * Get Api client
     *
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set Api client
     *
     * @param Client $client
     */
    public function setClient(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Is token persistent
     *
     * @return bool
     */
    public function isTokenSet()
    {
        $tokenString = $this->persistentContainer->get(self::CONTAINER_KEY_TOKEN);
        return !empty($tokenString);
    }

    /**
     * @return PersistentContainer
     */
    public function getPersistentContainer()
    {
        return $this->persistentContainer;
    }

    /**
     * Register user
     *
     * @param string $email
     * @param string $domain
     * @param string $locale
     * @param string $admin_url
     *
     * @return RegisterResponse
     */
    public function requestRegister($email, $domain, $locale, $admin_url = null)
    {
        $registerRequest = new RegisterRequest($email, $domain, $locale, $admin_url);
        $response = $this->client->sendRequest($registerRequest);
        return new RegisterResponse(new ProtectedResponse($response));
    }

    /**
     * Get shipping providers
     *
     * @return ShippingServices\GetShippingServicesListResponse
     */
    public function requestListShippingServices()
    {
        $listProvidersRequest = new GetListRequest($this->getToken());
        $response = $this->sendRequestWithInvalidTokenHandling($listProvidersRequest);
        /** @noinspection PhpMethodParametersCountMismatchInspection PHPStorm bug with wrapped traits */
        return new ShippingServices\GetShippingServicesListResponse(new ProtectedResponse($response));
    }


    /**
     * Get specific shipping provider
     *
     * @param int $serviceId
     * @return ShippingServices\GetShippingServiceResponse
     */
    public function requestGetShippingService($serviceId)
    {
        $getServiceRequest = new GetServiceRequest($this->getToken(), $serviceId);
        $response = $this->sendRequestWithInvalidTokenHandling($getServiceRequest);
        /** @noinspection PhpMethodParametersCountMismatchInspection PHPStorm bug with wrapped traits */
        return new ShippingServices\GetShippingServiceResponse(new ProtectedResponse($response));
    }


    /**
     * Creates shipment in service.
     *
     * @param ShipmentRequest $shipment
     * @param int $serviceId
     * @return PostShipmentResponse
     */
    public function requestPostShipment(ShipmentRequest $shipment, $serviceId)
    {
        $saasToken = new JWTSaasToken($this->getToken());

        $postShipmentRequest = new PostShipmentRequest($this->getToken(), $serviceId, $saasToken->getShopId(), $shipment);
        $response = $this->sendRequestWithInvalidTokenHandling($postShipmentRequest);
        /** @noinspection PhpMethodParametersCountMismatchInspection PHPStorm bug with wrapped traits */
        return new PostShipmentResponse(new ProtectedResponse($response));
    }

    /**
     * Rate shipment
     *
     * @param RateRequest $shipmentToRate
     * @param int $serviceId
     * @return PostRateResponse
     */
    public function requestPostRate(RateRequest $shipmentToRate, $serviceId)
    {
        $saasToken = new JWTSaasToken($this->getToken());

        $postRateRequest = new PostRateRequest($this->getToken(), $serviceId, $saasToken->getShopId(), $shipmentToRate);
        $response = $this->sendRequestWithInvalidTokenHandling($postRateRequest);
        /** @noinspection PhpMethodParametersCountMismatchInspection PHPStorm bug with wrapped traits */
        return new PostRateResponse(new ProtectedResponse($response));
    }

    /**
     * Creates document for shipment.
     *
     * @param ShipmentDocumentRequest $shipmentDocumentRequest
     * @param int $serviceId
     * @param int $shipmentId
     * @return PostShipmentDocumentResponse
     */
    public function requestPostShipmentDocument(
        ShipmentDocumentRequest $shipmentDocumentRequest,
        $serviceId,
        $shipmentId
    ) {
        $saasToken = new JWTSaasToken($this->getToken());

        $postShipmentDocumentRequest = new PostShipmentDocumentRequest(
            $this->getToken(),
            $shipmentId,
            $serviceId,
            $saasToken->getShopId(),
            $shipmentDocumentRequest
        );
        $response = $this->sendRequestWithInvalidTokenHandling($postShipmentDocumentRequest);
        /** @noinspection PhpMethodParametersCountMismatchInspection PHPStorm bug with wrapped traits */
        return new PostShipmentDocumentResponse(new ProtectedResponse($response));
    }

    /**
     * Deletes document from shipment.
     *
     * @param int $documentId
     * @return DeleteShipmentDocumentResponse
     */
    public function requestDeleteShipmentDocument(
        $documentId
    ) {
        $deleteShipmentDocumentRequest = new DeleteShipmentDocumentRequest(
            $this->getToken(),
            $documentId
        );
        $response = $this->sendRequestWithInvalidTokenHandling($deleteShipmentDocumentRequest);
        /** @noinspection PhpMethodParametersCountMismatchInspection PHPStorm bug with wrapped traits */
        return new DeleteShipmentDocumentResponse(new ProtectedResponse($response));
    }

    /**
     * Search Parcel Collection Points
     *
     * @param SearchRequest $searchParcelCollectionPointsRequest
     * @param int $serviceId
     * @return PostSearchResponse
     */
    public function requestPostSearchParcelCollectionPoints(
        SearchRequest $searchParcelCollectionPointsRequest,
        $serviceId
    ) {
        $saasToken = new JWTSaasToken($this->getToken());

        $postRateRequest = new PostSearchRequest(
            $this->getToken(),
            $serviceId,
            $saasToken->getShopId(),
            $searchParcelCollectionPointsRequest
        );
        $response        = $this->sendRequestWithInvalidTokenHandling($postRateRequest);

        /** @noinspection PhpMethodParametersCountMismatchInspection PHPStorm bug with wrapped traits */
        return new PostSearchResponse(new ProtectedResponse($response));
    }

    /**
     * Get single collection point
     *
     * @param SingleRequest $singleRequest
     * @param int $shippingServiceId
     * @return GetSingleResponse
     */
    public function requestGetSingleCollectionPoint(
        SingleRequest $singleRequest,
        $shippingServiceId
    ) {
        $saasToken = new JWTSaasToken($this->getToken());

        $getSingleRequest = new GetSingleRequest(
            $saasToken,
            $shippingServiceId,
            $saasToken->getShopId(),
            $singleRequest->id
        );
        $response         = $this->sendRequestWithInvalidTokenHandling($getSingleRequest);
        /** @noinspection PhpMethodParametersCountMismatchInspection PHPStorm bug with wrapped traits */
        return new GetSingleResponse(new ProtectedResponse($response));
    }

    /**
     * Get label. Request is POST because it can - and probably would change the shipment state in remote service
     *
     * @param int $shipmentId
     * @param int $serviceId
     * @return PostLabelResponse
     */
    public function requestPostLabel($shipmentId, $serviceId)
    {
        $saasToken = new JWTSaasToken($this->getToken());

        $postLabelRequest = new PostLabelRequest($this->getToken(), $serviceId, $saasToken->getShopId(), $shipmentId);
        $response = $this->sendRequestWithInvalidTokenHandling($postLabelRequest);
        /** @noinspection PhpMethodParametersCountMismatchInspection PHPStorm bug with wrapped traits */
        return new PostLabelResponse(new ProtectedResponse($response));
    }

    /**
     * Cancel shipment. Request is POST it change the shipment state in remote service
     *
     * @param int $shipmentId
     * @param int $serviceId
     * @return PostCancelResponse
     */
    public function requestPostCancel($shipmentId, $serviceId)
    {
        $saasToken = new JWTSaasToken($this->getToken());

        $postCancelRequest = new PostCancelRequest($this->getToken(), $serviceId, $saasToken->getShopId(), $shipmentId);
        $response = $this->sendRequestWithInvalidTokenHandling($postCancelRequest);
        /** @noinspection PhpMethodParametersCountMismatchInspection PHPStorm bug with wrapped traits */
        return new PostCancelResponse(new ProtectedResponse($response));
    }

    /**
     * Send documents
     *
     * @param int $shipmentId
     * @param int $serviceId
     * @return PostSendResponse
     */
    public function requestPostSendDocuments($shipmentId, $serviceId)
    {
        $saasToken = new JWTSaasToken($this->getToken());

        $postCancelRequest = new PostSendRequest($this->getToken(), $serviceId, $saasToken->getShopId(), $shipmentId);
        $response = $this->sendRequestWithInvalidTokenHandling($postCancelRequest);
        /** @noinspection PhpMethodParametersCountMismatchInspection PHPStorm bug with wrapped traits */
        return new PostSendResponse(new ProtectedResponse($response));
    }

    /**
     * Fetch dynamic fields
     *
     * @param int $serviceId
     * @param string[] $zoneTargets
     * @return GetFieldsResponse
     */
    public function requestGetFields($serviceId, array $zoneTargets)
    {
        $saasToken = new JWTSaasToken($this->getToken());

        $postCancelRequest = new GetFieldsRequest($this->getToken(), $serviceId, $saasToken->getShopId(), $zoneTargets);
        $response = $this->sendRequestWithInvalidTokenHandling($postCancelRequest);
        /** @noinspection PhpMethodParametersCountMismatchInspection PHPStorm bug with wrapped traits */
        return new GetFieldsResponse(new ProtectedResponse($response));
    }

    /**
     * Get settings
     *
     * @param int $shippingServiceId
     * @param string $type
     * @return GetSettingsResponse
     */
    public function requestGetSettings($shippingServiceId, $type = ShippingServiceSetting::TYPE_CONNECTION_SETTINGS)
    {
        $saasToken = new JWTSaasToken($this->getToken());

        $getSettingsRequest = new GetSettingsRequest($saasToken, $shippingServiceId, $saasToken->getShopId(), $type);
        $response = $this->sendRequestWithInvalidTokenHandling($getSettingsRequest);
        /** @noinspection PhpMethodParametersCountMismatchInspection PHPStorm bug with wrapped traits */
        return new GetSettingsResponse(new ProtectedResponse($response));
    }

    /**
     * Get status
     *
     * @return GetStatusResponse
     */
    public function requestGetStatus()
    {
        $saasToken = new JWTSaasToken($this->getToken());
        $getSettingsRequest = new GetStatusRequest($saasToken);
        $response = $this->sendRequestWithInvalidTokenHandling($getSettingsRequest);
        /** @noinspection PhpMethodParametersCountMismatchInspection PHPStorm bug with wrapped traits */
        return new GetStatusResponse(new ProtectedResponse($response));
    }

    /**
     * Save settings
     *
     * @param ShippingServiceSetting $setting
     * @return ProtectedResponse
     */
    public function requestSaveSettings(ShippingServiceSetting $setting)
    {
        $saasToken = new JWTSaasToken($this->getToken());
        $setting->setShop($saasToken->getShopId());

        $settingResponse = $this->requestGetSettings($setting->getShippingService(), $setting->getType());
        if ($settingResponse->isNotExists()) {
            $saveRequest = new PostSettingsRequest($saasToken, $setting);
        } else {
            $saveRequest = new PutSettingsRequest($saasToken, $setting);
        }

        return new ProtectedResponse($this->sendRequestWithInvalidTokenHandling($saveRequest));
    }

    /**
     * Test service connection status to verify service settings.
     *
     * @param int $shippingServiceId
     * @return GetSettingsTestResponse
     */
    public function requestGetSettingsTest($shippingServiceId)
    {
        $saasToken = new JWTSaasToken($this->getToken());

        $getSettingsTestRequest = new GetSettingsTestRequest($saasToken, $shippingServiceId, $saasToken->getShopId());
        $response = $this->sendRequestWithInvalidTokenHandling($getSettingsTestRequest);
        /** @noinspection PhpMethodParametersCountMismatchInspection PHPStorm bug with wrapped traits */
        return new GetSettingsTestResponse(new ProtectedResponse($response));
    }

    /**
     * Returns Token for Requests
     *
     * @return Token
     */
    protected function getToken()
    {
        try {
            $token = unserialize($this->persistentContainer->get(self::CONTAINER_KEY_TOKEN));
            if ($token instanceof Token) {
                $this->logger->notice('Took token from persistence container', $this->getLoggerContext());
                return $token;
            }
            $this->logger->info('Invalid token deserialization', $this->getLoggerContext());
            return new NullToken();
        } catch (ElementNotExistsException $e) {
            $this->logger->info('Token element does not exists in persisten container', $this->getLoggerContext());
            return new NullToken();
        }
    }

    /**
     * Send request and if token is invalid try to claim a new token
     *
     * @param AuthRequest $request
     * @return Response
     */
    protected function sendRequestWithInvalidTokenHandling(AuthRequest $request)
    {
        $isTokenValid = !$request->isTokenExpired() && !$request->isTokenInvalid();

        if ($isTokenValid) {
            /** @noinspection PhpMethodParametersCountMismatchInspection PHPStorm bug with wrapped traits */
            $response = new AuthApiResponse($this->client->sendRequest($request));
        }

        /** @noinspection PhpUndefinedVariableInspection Because if token was valid then some response must exists */
        if (!$isTokenValid || $response->isBadCredentials()) {
            $this->logger->info("Automatic token reclaim VALIDITY: {$isTokenValid}", $this->getLoggerContext());
            $tokenResponse = $this->requestToken(
                $this->persistentContainer->get(self::CONTAINER_KEY_CONNECT_KEY),
                $this->persistentContainer->get(self::CONTAINER_KEY_DOMAIN),
                $this->persistentContainer->get(self::CONTAINER_KEY_LOCALE)
            );

            if ($tokenResponse->isLoginSuccess()) {
                $request->setToken($tokenResponse->getToken());
                $response = $this->client->sendRequest($request);
            } else {
                $this->logger->notice('Tried to relogin but failed', $this->getLoggerContext());
                $response = $tokenResponse;
            }

        }
        return $response;
    }

    /**
     * Login user to get a token
     *
     * @param string $connectKey
     * @param string $domain
     * @param string $locale
     * @return TokenResponse
     */
    public function requestToken($connectKey, $domain, $locale)
    {
        $registerRequest = new TokenRequest($connectKey, $domain, $locale);
        $response = $this->client->sendRequest($registerRequest);
        $tokenResponse = new TokenResponse(new ProtectedResponse($response));
        if ($tokenResponse->isLoginSuccess()) {
            $this->setToken($tokenResponse->getToken());
            $this->persistentContainer->set(self::CONTAINER_KEY_CONNECT_KEY, $connectKey);
            $this->persistentContainer->set(self::CONTAINER_KEY_DOMAIN, $domain);
            $this->persistentContainer->set(self::CONTAINER_KEY_LOCALE, $locale);
        }

        return $tokenResponse;
    }

    /**
     * Remember token
     *
     * @param Token $token
     */
    public function setToken(Token $token)
    {
        $this->persistentContainer->set(self::CONTAINER_KEY_TOKEN, serialize($token));
        $this->logger->notice('Token save in persistence container', $this->getLoggerContext());
    }

    /**
     * Get user
     *
     * @param $saas_user_id
     *
     * @return GetUserResponse
     */
    public function requestGetUser($saas_user_id)
    {
        $getUserRequest = new GetUserRequest($saas_user_id, $this->getToken());
        $response = $this->sendRequestWithInvalidTokenHandling($getUserRequest);
        /** @noinspection PhpMethodParametersCountMismatchInspection PHPStorm bug with wrapped traits */
        return new GetUserResponse(new ProtectedResponse($response));
    }

    /**
     * Sets logger
     *
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Returns logger context for
     *
     * @param string $additional_context Optional additional context
     * @return array
     */
    protected function getLoggerContext($additional_context = '')
    {
        $context = [
            self::LIBARY_LOGIN_CONTEXT,
            self::class
        ];
        if ($additional_context !== '') {
            $context[] = $additional_context;
        }
        return $context;
    }


}