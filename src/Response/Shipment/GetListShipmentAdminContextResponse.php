<?php

namespace WPDesk\SaasPlatformClient\Response\Shipment;

use WPDesk\SaasPlatformClient\Model\Shipment\ShipmentAdminContext;
use WPDesk\SaasPlatformClient\Response\ApiResponse;
use WPDesk\SaasPlatformClient\Response\Traits\AuthApiResponseDecorator;
use WPDesk\SaasPlatformClient\Response\Traits\PagedListImplementation;

class GetListShipmentAdminContextResponse implements ApiResponse
{
    use AuthApiResponseDecorator;
    use PagedListImplementation;

    /*
     * @return ShippingService[]
     */
    public function getPage()
    {
        $page = $this->getRawPage();
        $itemList = [];
        if (count($page) > 0) {
            foreach ($page as $item) {
                $itemList[] = new ShipmentAdminContext($item);
            }
        }
        return $itemList;
    }
}