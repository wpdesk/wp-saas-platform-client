<?php

namespace WPDesk\SaasPlatformClient\Response\Rate;

use WPDesk\SaasPlatformClient\Model\Rate\RateResponse;
use WPDesk\SaasPlatformClient\Response\ApiResponse;
use WPDesk\SaasPlatformClient\Response\Traits\AuthApiResponseDecorator;

class PostRateResponse implements ApiResponse
{
    use AuthApiResponseDecorator;

    const RESPONSE_CODE_RATE_FAILED = 274;

    /**
     * @return RateResponse
     */
    public function getRate()
    {
        return new RateResponse($this->getResponseBody());
    }

    /**
     * Platform can't respond in meaningful way about rates
     *
     * @return bool
     */
    public function isRateFailedError()
    {
        return $this->rawResponse->getResponseCode() === self::RESPONSE_CODE_RATE_FAILED;
    }
}
