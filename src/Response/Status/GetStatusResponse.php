<?php

namespace WPDesk\SaasPlatformClient\Response\Status;

use WPDesk\SaasPlatformClient\Model\Status;
use WPDesk\SaasPlatformClient\Response\ApiResponse;
use WPDesk\SaasPlatformClient\Response\Traits\AuthApiResponseDecorator;

class GetStatusResponse implements ApiResponse
{
    use AuthApiResponseDecorator;

    /**
     * @return Status
     */
    public function getStatus()
    {
        return new Status($this->getResponseBody());
    }
}
