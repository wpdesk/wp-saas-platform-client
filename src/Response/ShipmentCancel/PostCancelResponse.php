<?php

namespace WPDesk\SaasPlatformClient\Response\ShipmentCancel;

use WPDesk\SaasPlatformClient\Response\ApiResponse;
use WPDesk\SaasPlatformClient\Response\Traits\AuthApiResponseDecorator;

class PostCancelResponse implements ApiResponse
{
    use AuthApiResponseDecorator;
}
