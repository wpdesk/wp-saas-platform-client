<?php

namespace WPDesk\SaasPlatformClient\Response\ShipmentDocument;

use WPDesk\SaasPlatformClient\Response\ApiResponse;
use WPDesk\SaasPlatformClient\Response\Traits\AuthApiResponseDecorator;

class DeleteShipmentDocumentResponse implements ApiResponse
{
    use AuthApiResponseDecorator;
}