<?php

namespace WPDesk\SaasPlatformClient\Response\ShippingServices;

use WPDesk\SaasPlatformClient\Model\ShippingService;
use WPDesk\SaasPlatformClient\Response\ApiResponse;
use WPDesk\SaasPlatformClient\Response\Traits\AuthApiResponseDecorator;

final class GetShippingServiceResponse implements ApiResponse
{
    use AuthApiResponseDecorator;

    /**
     * @return ShippingService
     */
    public function getShippingService()
    {
        return new ShippingService($this->getResponseBody());
    }
}