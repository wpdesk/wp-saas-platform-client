<?php

namespace WPDesk\SaasPlatformClient\Response\ShippingServices;

use WPDesk\SaasPlatformClient\Model\ShippingService;
use WPDesk\SaasPlatformClient\Response\ApiResponse;
use WPDesk\SaasPlatformClient\Response\Traits\AuthApiResponseDecorator;
use WPDesk\SaasPlatformClient\Response\Traits\PagedListImplementation;

final class GetShippingServicesListResponse implements ApiResponse
{
    use AuthApiResponseDecorator;
    use PagedListImplementation;

    /*
     * @return ShippingService[]
     */
    public function getPage()
    {
        $page = $this->getRawPage();
        $itemList = [];
        if (count($page) > 0) {
            foreach ($page as $item) {
                $itemList[] = new ShippingService($item);
            }
        }
        return $itemList;
    }

}