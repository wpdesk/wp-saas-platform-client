<?php

namespace WPDesk\SaasPlatformClient\Response\ParcelCollectionPoints;

use WPDesk\SaasPlatformClient\Model\ParcelCollectionPoints\SingleResponse;
use WPDesk\SaasPlatformClient\Response\ApiResponse;
use WPDesk\SaasPlatformClient\Response\Traits\AuthApiResponseDecorator;

final class GetSingleResponse implements ApiResponse
{

    use AuthApiResponseDecorator;

    /**
     * @return SingleResponse
     */
    public function getSingleResponse()
    {
        return new SingleResponse($this->getResponseBody());
    }


}