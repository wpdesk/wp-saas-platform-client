<?php

namespace WPDesk\SaasPlatformClient\Response\Label;

use WPDesk\SaasPlatformClient\Model\Label\Label;
use WPDesk\SaasPlatformClient\Response\ApiResponse;
use WPDesk\SaasPlatformClient\Response\Traits\AuthApiResponseDecorator;

class PostLabelResponse implements ApiResponse
{
    use AuthApiResponseDecorator;

    /**
     * @return Label
     */
    public function getLabel()
    {
        return new Label($this->getResponseBody());
    }
}
