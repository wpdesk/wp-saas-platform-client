<?php

namespace WPDesk\SaasPlatformClient\Response;

use WPDesk\SaasPlatformClient\Response\Exception\TriedExtractDataFromErrorResponse;
use WPDesk\SaasPlatformClient\Response\Traits\ApiResponseDecorator;

/**
 * Response is protected in a way so when you try to get body of the response when an error occured you will get an exception
 *
 * Class ProtectedResponse
 * @package WPDesk\SaasPlatformClient\Response
 */
class ProtectedResponse implements Response
{
    use ApiResponseDecorator;

    public function getResponseBody()
    {
        if ($this->isError()) {
            throw TriedExtractDataFromErrorResponse::createWithClassInfo(get_class($this->rawResponse), $this->getResponseCode());
        }
        return $this->rawResponse->getResponseBody();
    }


}