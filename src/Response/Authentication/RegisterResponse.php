<?php

namespace WPDesk\SaasPlatformClient\Response\Authentication;

use WPDesk\SaasPlatformClient\Response\ApiResponse;
use WPDesk\SaasPlatformClient\Response\RawResponse;
use WPDesk\SaasPlatformClient\Response\Traits\ApiResponseDecorator;

final class RegisterResponse implements ApiResponse
{
    const API_KEY_FIELD = 'password';

    const RESPONSE_CODE_USER_ALREADY_EXISTS = 460;

    const RESPONSE_CODE_DOMAIN_ALREADY_EXISTS = 461;

    use ApiResponseDecorator;

    /**
     * Returns true if user is registered
     *
     * @return bool
     */
    public function isUserRegistered()
    {
        return in_array($this->getResponseCode(),
            [RawResponse::RESPONSE_CODE_CREATED, RawResponse::RESPONSE_CODE_SUCCESS]);
    }

    /**
     * Returns true if user is was already registered
     *
     * @return bool
     */
    public function isUserAlreadyExists()
    {
        return $this->getResponseCode() === self::RESPONSE_CODE_USER_ALREADY_EXISTS;
    }

    /**
     * Returns true if user cannot be registered because domain is occupied
     *
     * @return bool
     */
    public function isDomainOccupied()
    {
        return $this->getResponseCode() === self::RESPONSE_CODE_DOMAIN_ALREADY_EXISTS;
    }

    /**
     * Returns apiKey from register response
     *
     * @return string
     */
    public function getApiKey()
    {
        return $this->getResponseBody()[self::API_KEY_FIELD];
    }
}