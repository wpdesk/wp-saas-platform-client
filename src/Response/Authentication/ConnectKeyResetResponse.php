<?php

namespace WPDesk\SaasPlatformClient\Response\Authentication;

use WPDesk\SaasPlatformClient\Response\ApiResponse;
use WPDesk\SaasPlatformClient\Response\Traits\AuthApiResponseDecorator;

final class ConnectKeyResetResponse implements ApiResponse
{
    use AuthApiResponseDecorator;

    /**
     * Returns newly generated connect key
     *
     * @return string
     */
    public function getNewConnectKey()
    {
        $body = $this->getResponseBody();
        return (string)$body['connectKey'];
    }
}