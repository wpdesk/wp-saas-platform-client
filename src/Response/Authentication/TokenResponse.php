<?php

namespace WPDesk\SaasPlatformClient\Response\Authentication;

use WPDesk\SaasPlatformClient\Authentication\JWTToken;
use WPDesk\SaasPlatformClient\Response\ApiResponse;
use WPDesk\SaasPlatformClient\Response\AuthApiResponse;
use WPDesk\SaasPlatformClient\Response\Traits\ApiResponseDecorator;

final class TokenResponse implements ApiResponse
{
    use ApiResponseDecorator;

    const RESPONSE_CODE_LOGIN_SUCCESS = 200;

    /**
     * Returns token if exists
     *
     * @return JWTToken
     */
    public function getToken()
    {
        $body = $this->getResponseBody();
        return new JWTToken($body['token']);
    }

    /**
     * @return bool
     */
    public function isBadCredentials()
    {
        return $this->getResponseCode() === AuthApiResponse::RESPONSE_CODE_BAD_CREDENTIALS;
    }

    /**
     * Returns true if login is a success and token is available
     *
     * @return bool
     */
    public function isLoginSuccess()
    {
        return $this->getResponseCode() === self::RESPONSE_CODE_LOGIN_SUCCESS;
    }

    /**
     * Returns true if user cannot be registered because domain is occupied
     *
     * @return bool
     */
    public function isDomainOccupied()
    {
        return $this->getResponseCode() === RegisterResponse::RESPONSE_CODE_DOMAIN_ALREADY_EXISTS;
    }


}