<?php

namespace WPDesk\SaasPlatformClient\Response;

interface Response
{
    /**
     * @return int
     */
    public function getResponseCode();

    /** @return array */
    public function getResponseBody();

    /** @return array */
    public function getResponseHeaders();

    /** @return array */
    public function getResponseErrorBody();

    /**
     * Returns response error message if present or null.
     *
     * @return string|null
     */
    public function getResponseMessage();

    /**
     * Is any error occured
     *
     * @return bool
     */
    public function isError();

    /**
     * Is maintenance
     *
     * @return bool
     */
    public function isMaintenance();

    /**
     * Get platform version hash string.
     *
     * @return bool|string
     */
    public function getPlatformVersionHash();

}