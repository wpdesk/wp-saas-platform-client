<?php

namespace WPDesk\SaasPlatformClient\Response\ShippingServicesSettings\Test;

use WPDesk\SaasPlatformClient\Response\ApiResponse;
use WPDesk\SaasPlatformClient\Response\Exception\EmptyStatusFromResponse;
use WPDesk\SaasPlatformClient\Response\Traits\AuthApiResponseDecorator;

class GetSettingsTestResponse implements ApiResponse
{
    use AuthApiResponseDecorator;

    const MESSAGE = 'message';
    const STATUS  = 'status';

    public function getMessage()
    {
        $responseBody = $this->getResponseBody();
        if (isset($responseBody[self::MESSAGE])) {
            return $responseBody[self::MESSAGE];
        } else {
            throw EmptyMessageFromResponse::createWithClassInfo(get_class($this->rawResponse), $this->getResponseCode());
        }
    }

    public function getStatus()
    {
        $responseBody = $this->getResponseBody();
        if (isset($responseBody[self::STATUS])) {
            return $responseBody[self::STATUS];
        } else {
            throw EmptyStatusFromResponse::createWithClassInfo(get_class($this->rawResponse), $this->getResponseCode());
        }
    }

}
