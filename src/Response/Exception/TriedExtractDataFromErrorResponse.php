<?php

namespace WPDesk\SaasPlatformClient\Response\Exception;

class TriedExtractDataFromErrorResponse extends \RuntimeException
{
    /**
     * Exception factory
     *
     * @param string $request
     * @param int $errorCode
     * @return TriedExtractDataFromErrorResponse
     */
    public static function createWithClassInfo($request, $errorCode)
    {
        return new TriedExtractDataFromErrorResponse("Tried to extract data from {$request} when an error occured. Response code: {$errorCode}",
            $errorCode);
    }
}