<?php
namespace WPDesk\SaasPlatformClient\HttpClient;

/**
 * Interface for all exceptions that can be thrown by HttpClient
 *
 * @package WPDesk\SaasPlatformClient\HttpClient
 */
interface HttpClientRequestException
{

}