<?php

namespace WPDesk\SaasPlatformClient\HttpClient\Curl\Exception;

use WPDesk\SaasPlatformClient\HttpClient\HttpClientRequestException;

/**
 * Base class for all curl exceptions.
 *
 * @package WPDesk\SaasPlatformClient\HttpClient\Curl\Exception
 */
class CurlException extends \RuntimeException implements HttpClientRequestException
{

}