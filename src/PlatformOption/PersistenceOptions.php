<?php

namespace WPDesk\SaasPlatformClient\PlatformOption;

interface PersistenceOptions
{
    /**
     * @return string
     */
    public function getPersistenceClass();
}