<?php

namespace WPDesk\SaasPlatformClient\PlatformOption;

interface PlatformUrlOptions
{
    /**
     * @return string
     */
    public function getApiUrl();
}