<?php

namespace WPDesk\SaasPlatformClient\Persistence;

use WPDesk\Persistence\PersistentContainer;
use WPDesk\SaasPlatformClient\PlatformOption\PersistenceOptions;

class PersistentContainerFactory
{
    /**
     * @param PersistenceOptions $options
     * @return PersistentContainer
     */
    public function createContainer(PersistenceOptions $options)
    {
        $className = $options->getPersistenceClass();
        return new $className;
    }
}