<?php

namespace WPDesk\SaasPlatformClient;

use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use WPDesk\Persistence\MemoryContainer;
use WPDesk\SaasPlatformClient\HttpClient\Curl\CurlClient;
use WPDesk\SaasPlatformClient\PlatformOption\PlatformUrlOptions;
use WPDesk\SaasPlatformClient\PlatformOption\HttpClientOptions;
use WPDesk\SaasPlatformClient\PlatformOption\PersistenceOptions;
use WPDesk\SaasPlatformClient\PlatformOption\SerializerOptions;
use WPDesk\SaasPlatformClient\Serializer\JsonSerializer;

class PlatformFactoryOptions implements PersistenceOptions, HttpClientOptions, SerializerOptions, PlatformUrlOptions
{

    const DEFAULT_CACHE_TTL = 600;

    /**
     * @var string
     */
    private $httpClientClass = CurlClient::class;

    /**
     * @var string
     */
    protected $persistenceClass = MemoryContainer::class;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $serializerClass = JsonSerializer::class;

    /**
     * @var string
     */
    private $apiUrl = 'https://app.flexibleshipping.com/api/v1';

    /** @var bool  */
    protected $cachedClient = false;

    /** @var int  */
    protected $cacheTtl = self::DEFAULT_CACHE_TTL;

    /**
     * @var string
     */
    private $apiMimeType = 'application/json';

    /**
     * Default request headers - added to each request.
     *
     * @var array
     */
    private $defaultRequestHeaders = array();

    /**
     * PlatformFactoryOptions constructor.
     */
    public function __construct()
    {
        if (getenv('SAAS_FLEXIBLESHIPPING_URL')) {
            $this->apiUrl = getenv('SAAS_FLEXIBLESHIPPING_URL');
        }
        if (defined('SAAS_FLEXIBLESHIPPING_URL')) {
            $this->apiUrl = SAAS_FLEXIBLESHIPPING_URL;
        }
        $this->logger = new NullLogger();
    }

    /**
     * @return string
     */
    public function getHttpClientClass()
    {
        return $this->httpClientClass;
    }

    /**
     * @param string $httpClientClass
     */
    public function setHttpClientClass($httpClientClass)
    {
        $this->httpClientClass = $httpClientClass;
    }

    /**
     * @return string
     */
    public function getPersistenceClass()
    {
        return $this->persistenceClass;
    }

    /**
     * @param string $persistenceClass
     */
    public function setPersistenceClass($persistenceClass)
    {
        $this->persistenceClass = $persistenceClass;
    }

    /**
     * @return string
     */
    public function getSerializerClass()
    {
        return $this->serializerClass;
    }

    /**
     * @param string $serializerClass
     */
    public function setSerializerClass($serializerClass)
    {
        $this->serializerClass = $serializerClass;
    }

    /**
     * @return string
     */
    public function getApiUrl()
    {
        return $this->apiUrl;
    }

    /**
     * @param string $apiUrl
     */
    public function setApiUrl($apiUrl)
    {
        $this->apiUrl = $apiUrl;
    }

    /**
     * @return string
     */
    public function getApiMimeType()
    {
        return $this->apiMimeType;
    }

    /**
     * @param string $apiMimeType
     */
    public function setApiMimeType($apiMimeType)
    {
        $this->apiMimeType = $apiMimeType;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

    /**
     * @return bool
     */
    public function isCachedClient()
    {
        return $this->cachedClient;
    }

    /**
     * @param bool $cachedClient
     */
    public function setCachedClient($cachedClient)
    {
        $this->cachedClient = $cachedClient;
    }

    /**
     * @param int $cacheTtl
     */
    public function setCacheTtl($cacheTtl)
    {
        $this->cacheTtl = $cacheTtl;
    }

    /**
     * @return int
     */
    public function getCacheTtl()
    {
        return $this->cacheTtl;
    }

    /**
     * @return array
     */
    public function getDefaultRequestHeaders()
    {
        return $this->defaultRequestHeaders;
    }

    /**
     * @param array $defaultRequestHeaders
     */
    public function setDefaultRequestHeaders($defaultRequestHeaders)
    {
        $this->defaultRequestHeaders = $defaultRequestHeaders;
    }



}