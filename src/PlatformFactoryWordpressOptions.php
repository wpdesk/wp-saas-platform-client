<?php

namespace WPDesk\SaasPlatformClient;

use WPDesk\Persistence\Wordpress\WordpressOptionsContainer;

class PlatformFactoryWordpressOptions extends PlatformFactoryOptions
{
    /** @var bool  */
    protected $cachedClient = false;

    /**
     * @var string
     */
    protected $persistenceClass = WordpressOptionsContainer::class;
}