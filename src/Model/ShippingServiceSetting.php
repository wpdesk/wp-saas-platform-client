<?php

namespace WPDesk\SaasPlatformClient\Model;

class ShippingServiceSetting extends AbstractModel
{
    const TYPE_CONNECTION_SETTINGS = 'connection';

    /** @var array */
    protected $json_value;

    /** @var int|string */
    protected $shipping_service;

    /** @var int|string */
    protected $shop;

    /** @var string */
    protected $type;

    public function __construct(array $data = null)
    {
        parent::__construct($data);
        $this->type = self::TYPE_CONNECTION_SETTINGS;
    }

    /**
     * @return array
     */
    public function getJsonValue()
    {
        return $this->json_value;
    }

    /**
     * @param array|\stdClass $json_value
     */
    public function setJsonValue($json_value)
    {
        if ($json_value instanceof \stdClass) {
            $this->json_value = get_object_vars($json_value);
        } else {
            $this->json_value = $json_value;
        }
        $this->json_value = $this->clear_non_assoc_settings($this->json_value);
    }

    /**
     * Clear values that are not part of associative array
     *
     * @param array $json_value
     *
     * @return array
     */
    private function clear_non_assoc_settings(array $json_value) {
        return array_filter($json_value, function($key) {
            return !is_int($key);
        }, ARRAY_FILTER_USE_KEY);
    }

    /**
     * @return int|string
     */
    public function getShippingService()
    {
        return $this->shipping_service;
    }

    /**
     * @param int|string $shipping_service
     */
    public function setShippingService($shipping_service)
    {
        $this->shipping_service = $shipping_service;
    }

    /**
     * @return int|string
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param int|string $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

}


