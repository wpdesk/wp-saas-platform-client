<?php

namespace WPDesk\SaasPlatformClient\Model\ParcelCollectionPoints;

use WPDesk\SaasPlatformClient\Model\AbstractModel;

class SingleResponse extends AbstractModel
{

    /**
     * @var ClientCollectionPoint
     */
    public $point;

    /** @var array */
    public $messageStack = [];


    /**
     * From array.
     *
     * @param array $data Data.
     */
    public function fromArray(array $data)
    {
        if (isset($data['point'])) {
            $this->point = new ClientCollectionPoint($data['point']);
        }
        if (isset($data['messageStack'])) {
            $this->messageStack = $data['messageStack'];
        }
    }

}
