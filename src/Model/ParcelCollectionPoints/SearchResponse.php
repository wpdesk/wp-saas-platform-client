<?php

namespace WPDesk\SaasPlatformClient\Model\ParcelCollectionPoints;

use WPDesk\SaasPlatformClient\Model\AbstractModel;

class SearchResponse extends AbstractModel
{

    /**
     * @var ClientCollectionPoint[]
     */
    public $points = [];

    /** @var array */
    public $messageStack = [];

    /**
     * From array.
     *
     * @param array $data Data.
     */
    public function fromArray(array $data)
    {
        if ($data['points'] && count($data['points']) > 0) {
            foreach ($data['points'] as $point) {
                $this->points[] = new ClientCollectionPoint($point);
            }
        }
        if (isset($data['messageStack'])) {
            $this->messageStack = $data['messageStack'];
        }
    }
}