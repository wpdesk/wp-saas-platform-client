<?php

namespace WPDesk\SaasPlatformClient\Model;

use \ArrayAccess;
use ReflectionProperty;

/**
 * Base class for models.
 *
 * It's much simplified but works. So here is the basic idea:
 * - we have a json data presented/decoded as array
 * - we want to denormalize the data and hydrate out object with them.
 *
 * We use fromArray method and:
 * - we tries to check if protected(!)/public property with array key name exists, we check also camel case version of the var
 * - if property exists we try to use Setter first so if you want to intercept serialization of model you can add protected setter
 * - if setter not exists and the current value is an AbstractModel then we try to use fromArray on it
 * - if all fails we just use simple assign to the property val
 *
 * @package WPDesk\SaasPlatformClient\Model
 */
class AbstractModel implements ArrayAccess
{
    /**
     * Constructor
     * @param mixed[] $data Associated array of property value initalizing the model
     */
    public function __construct(array $data = null)
    {
        if ($data !== null) {
            $this->fromArray($data);
        }
    }

    /**
     * Hydrate object from array.
     * Try to use snake case then camel case.
     *
     * @param array $data
     */
    public function fromArray(array $data)
    {
        $propertyNames = $this->extractPropertiesNames();

        foreach ($data as $key => $value) {
            if (in_array($key, $propertyNames, true)) {
                $this[$key] = $value;
            } else {
                $camelKey = $this->snakeToCamel($key);
                if (in_array($camelKey, $propertyNames, true)) {
                    $this[$camelKey] = $value;
                }
            }
        }
        if (isset($data['_embedded']) && is_array($data['_embedded'])) {
            $this->fromArray($data['_embedded']);
        }
    }

    /**
     * @return string[]
     */
    protected function extractPropertiesNames()
    {
        try {
            $reflection = new \ReflectionClass($this);
            $properties = $reflection->getProperties();

            return array_map(function (ReflectionProperty $property) {
                return $property->getName();
            }, $properties);
        } catch (\ReflectionException $e) {
            return [];
        }
    }

    /**
     * Returns true if offset exists. False otherwise.
     * @param  string $offset Offset
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->extractPropertiesNames()[$offset]);
    }

    /**
     * Gets offset.
     * @param  string $offset Offset
     * @return mixed
     */
    public function offsetGet($offset)
    {
        if (isset($this->$offset)) {
            $getterName = $this->propertyNameToMethod($offset, 'get');
            if (method_exists($this, $getterName)) {
                return $this->$getterName();
            }
            return $this->$offset;
        }

        return null;

    }

    /**
     * @param string $name
     * @param string $prefix
     * @return string
     */
    private function propertyNameToMethod($name, $prefix)
    {
        return $prefix . ucfirst($this->snakeToCamel($name));
    }

    /**
     * Convert snake case to camel case
     *
     * @param $str
     * @return string
     */
    private function snakeToCamel($str)
    {
        // Remove underscores, capitalize words, squash, lowercase first.
        return lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $str))));
    }

    /**
     * Unsets offset.
     * @param  string $offset Offset
     * @return void
     */
    public function offsetUnset($offset)
    {
        $this->offsetSet($offset, null);
    }

    /**
     * Sets value based on offset. It's tries to use setter method first.
     * If setter is not available and current value is an object then tries to hydrate it by using fromArray
     * In the end it's tries to use assigment.
     *
     * Note that if $value is null and $offset is an object then object will be overridden by null
     *
     * @param  string $offset Offset
     * @param  mixed $value Value to be set
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (null !== $offset) {
            $setterName = $this->propertyNameToMethod($offset, 'set');
            try {
                if (method_exists($this, $setterName)) {
                    $this->$setterName($value);
                } else {
                    if ($value !== null && isset($this->$offset) && is_object($this->$offset) && $this->$offset instanceof AbstractModel) {
                        $this->$offset->fromArray($value);
                    } else {
                        $this->$offset = $value;
                    }
                }
            } catch (\Exception $e) {
                // well that is for a greater good. We know that setter failed horribly so lets try further
            }
        }
    }

    /**
     * Hydrate object from stdClass
     *
     * @param \stdClass $object
     */
    public function fromObject(\stdClass $object)
    {
        foreach (get_object_vars($object) as $key => $value) {
            $this[$key] = $value;
        }
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        $encoded = json_encode($this->toArray());
        return $encoded? (string) $encoded: '';
    }


    /**
     * Convert object to array.
     *
     * @param mixed $iterable
     *
     * @return array
     */
    private function toArrayFromIterable($iterable)
    {
        $array = [];
        foreach ($iterable as $key => $value) {
            if ($value instanceof AbstractModel) {
                $array[$key] = $value->toArray();
            } elseif ($value instanceof \DateTime) {
                $array[$key] = $value->format('c');
            } elseif (is_array($value)) {
                $array[$key] = $this->toArrayFromIterable($value);
            } else {
                $array[$key] = $value;
            }
        }
        return $array;
    }


    /**
     * Convert object to array
     *
     * @return array
     */
    public function toArray()
    {
        return $this->toArrayFromIterable($this);
    }
}


