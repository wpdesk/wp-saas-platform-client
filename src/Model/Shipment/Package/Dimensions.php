<?php


namespace WPDesk\SaasPlatformClient\Model\Shipment\Package;

use WPDesk\SaasPlatformClient\Model\AbstractModel;

class Dimensions extends AbstractModel
{
    /** @var int */
    protected $height;

    /** @var int */
    protected $width;

    /** @var int */
    protected $length;

    /**
     * @param int $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @param int $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @param int $length
     */
    public function setLength($length)
    {
        $this->length = $length;
    }
}