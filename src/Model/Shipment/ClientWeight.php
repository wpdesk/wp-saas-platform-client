<?php

namespace WPDesk\SaasPlatformClient\Model\Shipment;

use WPDesk\SaasPlatformClient\Model\AbstractModel;

/**
 * Class ClientMoney
 * Compatible with Fowler Money.
 * @see http://moneyphp.org/en/stable
 * @package WPDesk\SaasPlatformClient\Model\Shipment
 */
final class ClientWeight extends AbstractModel
{
    /**
     **
     * Weight KGS by default
     *
     * @Constraints\NotBlank
     * @Constraints\GreaterThanOrEqual(0)
     *
     * @var int
     */
    protected $amount;

    /**
     * @Constraints\NotBlank
     * @Constraints\Choice({"G"})
     *
     * @var string
     */
    protected $unit;

    /**
     * @param int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * @param string $unit
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
    }

}