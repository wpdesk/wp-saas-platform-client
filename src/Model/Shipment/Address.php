<?php


namespace WPDesk\SaasPlatformClient\Model\Shipment;


use WPDesk\SaasPlatformClient\Model\AbstractModel;

class Address extends AbstractModel
{
    /**
     * @Constraints\NotBlank
     *
     * @var string
     */
    public $addressLine1;

    /** @var string */
    public $addressLine2;

    /**
     * @Constraints\NotBlank
     *
     * @var string
     */
    public $postalCode;

    /**
     * @Constraints\NotBlank
     *
     * @var string
     */
    public $city;

    /**
     * @var string
     */
    public $stateCode;

    /**
     * @Constraints\NotBlank
     *
     * @var string
     */
    public $countryCode;

    /**
     * @return string
     */
    public function getAddressLine1()
    {
        return $this->addressLine1;
    }

    /**
     * @return string
     */
    public function getAddressLine2()
    {
        return $this->addressLine2;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getStateCode()
    {
        return $this->stateCode;
    }

    /**
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * @param string $addressLine1
     */
    public function setAddressLine1($addressLine1)
    {
        $this->addressLine1 = $addressLine1;
    }

    /**
     * @param string $addressLine2
     */
    public function setAddressLine2($addressLine2)
    {
        $this->addressLine2 = $addressLine2;
    }

    /**
     * @param string $postalCode
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @param string $stateCode
     */
    public function setStateCode($stateCode)
    {
        $this->stateCode = $stateCode;
    }

    /**
     * @param string $countryCode
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;
    }
}