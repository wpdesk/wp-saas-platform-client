<?php


namespace WPDesk\SaasPlatformClient\Model\Shipment;


use WPDesk\SaasPlatformClient\Model\AbstractModel;

class ShopFlow extends AbstractModel
{

    /**
     * Created via.
     *
     * @var string
     */
    protected $createdVia;

    /**
     * Sent via.
     *
     * @var string
     */
    protected $sentVia;

    /**
     * Fallback.
     *
     * @var bool
     */
    protected $fallback = false;

    /**
     * Fallback message.
     *
     * @var string
     */
    protected $fallbackMessage;

    /**
     * Rate type.
     *
     * @var string
     */
    protected $rateType;

    /**
     * @var string
     */
    protected $checkoutServiceType;

    /**
     * @var string
     */
    protected $checkoutServiceName;

    /**
     * @param string $createdVia
     */
    public function setCreatedVia($createdVia)
    {
        $this->createdVia = $createdVia;
    }

    /**
     * @param string $sentVia
     */
    public function setSentVia($sentVia)
    {
        $this->sentVia = $sentVia;
    }

    /**
     * @param bool $fallback
     */
    public function setFallback($fallback)
    {
        $this->fallback = $fallback;
    }

    /**
     * @param string $fallbackMessage
     */
    public function setFallbackMessage($fallbackMessage)
    {
        $this->fallbackMessage = $fallbackMessage;
    }

    /**
     * @param string $rateType
     */
    public function setRateType($rateType)
    {
        $this->rateType = $rateType;
    }

    /**
     * @param string $checkoutServiceType
     */
    public function setCheckoutServiceType($checkoutServiceType)
    {
        $this->checkoutServiceType = $checkoutServiceType;
    }

    /**
     * @param string $checkoutServiceName
     */
    public function setCheckoutServiceName($checkoutServiceName)
    {
        $this->checkoutServiceName = $checkoutServiceName;
    }

}