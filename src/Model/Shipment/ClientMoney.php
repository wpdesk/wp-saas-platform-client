<?php

namespace WPDesk\SaasPlatformClient\Model\Shipment;

use WPDesk\SaasPlatformClient\Model\AbstractModel;

/**
 * Class ClientMoney
 * Compatible with Fowler Money.
 * @see http://moneyphp.org/en/stable
 * @package WPDesk\SaasPlatformClient\Model\Shipment
 */
final class ClientMoney extends AbstractModel
{
    /**
     * Value in string field but usually is INT
     *
     * @var string
     */
    public $amount;

    /** @var string In ISO 4217 */
    public $currency;

    /**
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }
}