<?php


namespace WPDesk\SaasPlatformClient\Model\Shipment;

use WPDesk\SaasPlatformClient\Model\AbstractModel;
use WPDesk\SaasPlatformClient\Model\Shipment\Package\Dimensions;

class Package extends AbstractModel
{
    /**
     * Package type values may vary over different shipping service providers
     *
     * @Constraints\NotBlank
     *
     * @var string
     */
    protected $type;

    /**
     * Weight in grams
     *
     * @Constraints\NotBlank
     * @Constraints\GreaterThanOrEqual(0)
     *
     * @var int
     */
    protected $weight;

    /**
     * @Constraints\NotBlank
     * @Constraints\Choice({"KGS", "LBS", "G})
     *
     * @var string
     */
    protected $weightUnit = 'G';

    /**
     * @Constraints\Valid
     * @var Dimensions
     */
    public $dimensions;

    /**
     * @var string
     */
    public $dimensionsUnit = 'MM';

    /**
     * @Constraints\NotBlank
     *
     * @var string
     */
    protected $description;

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @param int $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @param string $weightUnit
     */
    public function setWeightUnit($weightUnit)
    {
        $this->weightUnit = $weightUnit;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
}