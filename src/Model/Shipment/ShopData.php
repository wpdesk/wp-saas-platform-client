<?php


namespace WPDesk\SaasPlatformClient\Model\Shipment;


use WPDesk\SaasPlatformClient\Model\AbstractModel;

class ShopData extends AbstractModel
{

    /**
     * Order id.
     *
     * @var int
     */
    protected $orderId;

    /**
     * Shipment id.
     *
     * @var int
     */
    protected $shipmentId;


    /**
     * @param int $orderId
     */
    public function setOrderId($orderId)
    {
        $this->orderId = $orderId;
    }

    /**
     * @param int $shipmentId
     */
    public function setShipmentId($shipmentId)
    {
        $this->shipmentId = $shipmentId;
    }

}