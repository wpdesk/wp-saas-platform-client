<?php

namespace WPDesk\SaasPlatformClient\Model;

class Shop extends AbstractModel
{

    /** @var int */
    protected $id;

    /** @var string */
    protected $domain;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }
}


