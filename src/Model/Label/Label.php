<?php

namespace WPDesk\SaasPlatformClient\Model\Label;

use WPDesk\SaasPlatformClient\Model\AbstractModel;

class Label extends AbstractModel
{
    /** @var string */
    protected $labelId;

    /** @var string */
    protected $labelContent;

    /** @var string */
    protected $labelContentMime = 'image/gif';

    /** @var string */
    protected $labelContentExt = 'pdf';

    /**
     * @return string
     */
    public function getLabelContent()
    {
        return $this->labelContent;
    }

    /**
     * @return string
     */
    public function getLabelContentMime()
    {
        return $this->labelContentMime;
    }

    /**
     * @return string
     */
    public function getLabelContentExt()
    {
        return $this->labelContentExt;
    }

}


