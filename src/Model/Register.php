<?php

namespace WPDesk\SaasPlatformClient\Model;

class Register extends AbstractModel
{
    /** @var string */
    protected $email;

    /** @var string */
    protected $domain;

    /** @var string */
    protected $locale;

    /** @var string */
    protected $admin_url;

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param string $domain
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
    }

    /**
     * @param string $admin_url
     */
    public function setAdminUrl($admin_url)
    {
        $this->admin_url = $admin_url;
    }
}


