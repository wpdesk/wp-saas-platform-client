<?php

namespace WPDesk\SaasPlatformClient\Model\Rate;

use WPDesk\SaasPlatformClient\Model\AbstractModel;
use WPDesk\SaasPlatformClient\Model\Shipment\ClientMoney;

class SingleRate extends AbstractModel
{
    /** string */
    public $serviceType;

    /** string */
    public $serviceName;

    /**
     * @var ClientMoney|null
     */
    public $transportCharge;

    /**
     * @var ClientMoney|null
     */
    public $serviceCharge;

    /**
     * @var ClientMoney|null
     */
    public $totalCharge;

    /**
     * @var int
     */
    public $daysToDelivery;

    public function __construct(array $data = null)
    {
        $this->transportCharge = new ClientMoney();
        $this->serviceCharge = new ClientMoney();
        $this->totalCharge = new ClientMoney();

        parent::__construct($data);
    }
}