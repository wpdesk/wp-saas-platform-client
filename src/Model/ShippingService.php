<?php

namespace WPDesk\SaasPlatformClient\Model;

class ShippingService extends AbstractModel
{
    /** @var int */
    protected $id;

    /** @var string */
    protected $name;

    /** @var string */
    protected $description;

    /** @var string */
    protected $logo_url;

    /** @var array */
    protected $connection_settings_definition_json;

    /** @var array */
    protected $connection_settings_values_json_schema;

    /** @var array */
    protected $request_fields_definition_json;

    /** @var array */
    protected $capabilities_json;

    /** @var bool */
    protected $enabled;

    /** @var bool */
    protected $promoted;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getLogoUrl()
    {
        return $this->logo_url;
    }

    /**
     * @return array
     */
    public function getConnectionSettingsDefinitionJson()
    {
        return $this->connection_settings_definition_json;
    }

    /**
     * @return array
     */
    public function getConnectionSettingsValuesJsonSchema()
    {
        return $this->connection_settings_values_json_schema;
    }

    /**
     * @return array
     */
    public function getRequestFieldsDefinitionJson()
    {
        return $this->request_fields_definition_json;
    }

    /**
     * @return array
     */
    public function getCapabilitiesJson()
    {
        return $this->capabilities_json;
    }

    /**
     * @return bool
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @return bool
     */
    public function getPromoted()
    {
        return $this->promoted;
    }

}


