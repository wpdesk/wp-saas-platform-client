<?php

namespace WPDesk\SaasPlatformClient\Model;

class User extends AbstractModel
{

    /** @var int */
    protected $id;

    /** @var string */
    protected $email;

    /** @var ShippingPlan */
    protected $shipping_plan;

    /** @var string[] */
    protected $roles;

    /** @var string */
    protected $password;

    public function __construct($data = null)
    {
        $this->shipping_plan = new ShippingPlan();
        parent::__construct($data);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return ShippingPlan
     */
    public function getShippingPlan()
    {
        return $this->shipping_plan;
    }

    /**
     * @return string[]
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
}


