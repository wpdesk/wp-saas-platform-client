<?php

namespace WPDesk\SaasPlatformClient\Model;

class KeyInfo extends AbstractModel
{
    /** @var \DateTime */
    protected $last_used;

    /** @var int */
    protected $owner_id;

    /** @var array */
    protected $domains;

    /**
     * @return \DateTime
     */
    public function getLastUsed()
    {
        return $this->last_used;
    }

    /**
     * @return int
     */
    public function getOwnerId()
    {
        return $this->owner_id;
    }

    /**
     * @return array
     */
    public function getDomains()
    {
        return $this->domains;
    }
}


