<?php

namespace WPDesk\SaasPlatformClient\Model;

class Reset extends AbstractModel
{
    /** @var string */
    protected $connect_key;

    /**
     * @param string $connect_key
     */
    public function setConnectKey($connect_key)
    {
        $this->connect_key = $connect_key;
    }
}


