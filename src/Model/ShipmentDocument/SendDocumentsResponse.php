<?php

namespace WPDesk\SaasPlatformClient\Model\ShipmentDocument;

use WPDesk\SaasPlatformClient\Model\AbstractModel;

class SendDocumentsResponse extends AbstractModel
{

    /**
     * @var array
     */
    protected $documentStatuses;

    /**
     * @return array
     */
    public function getDocumentStatuses()
    {
        return $this->documentStatuses;
    }

}