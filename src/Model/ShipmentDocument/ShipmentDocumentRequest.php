<?php

namespace WPDesk\SaasPlatformClient\Model\ShipmentDocument;

use WPDesk\SaasPlatformClient\Model\AbstractModel;

class ShipmentDocumentRequest extends AbstractModel
{

    /**
     * @var string document type.
     */
    protected $documentType;

    /**
     * @var string
     */
    protected $fileName;

    /**
     * Content encoded in base64
     * @var string
     */
    protected $fileContent;

    /**
     * @return string
     */
    public function getDocumentType()
    {
        return $this->documentType;
    }

    /**
     * @param string $documentType
     */
    public function setDocumentType($documentType)
    {
        $this->documentType = $documentType;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return string
     */
    public function getFileContent()
    {
        return $this->fileContent;
    }

    /**
     * @param string $fileContent
     */
    public function setFileContent($fileContent)
    {
        $this->fileContent = $fileContent;
    }

}