<?php

namespace WPDesk\SaasPlatformClient\Model;

class ShippingPlan extends AbstractModel
{
    /**
     * @var int Unique numeric ID.
     */
    protected $id;

    /**
     * @var integer Limit of maximum packages that can be sent.
     */
    protected $packageLimit;

    /**
     * @var \DateTimeInterface of when the plan has started.
     */
    protected $periodStart;

    /**
     * @var \DateTimeInterface Date and Time of when the plan ends.
     */
    protected $periodEnd;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getPackageLimit()
    {
        return $this->packageLimit;
    }

    /**
     * @param int $packageLimit
     */
    public function setPackageLimit($packageLimit)
    {
        $this->packageLimit = $packageLimit;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getPeriodStart()
    {
        return $this->periodStart;
    }

    /**
     * @param \DateTimeInterface $periodStart
     */
    public function setPeriodStart($periodStart)
    {
        $this->periodStart = $periodStart;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getPeriodEnd()
    {
        return $this->periodEnd;
    }

    /**
     * @param \DateTimeInterface $periodEnd
     */
    public function setPeriodEnd($periodEnd)
    {
        $this->periodEnd = $periodEnd;
    }
}


