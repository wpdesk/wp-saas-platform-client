<?php

namespace WPDesk\SaasPlatformClient\Model;

class Token extends AbstractModel
{
    /** @var string */
    protected $token;

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }
}


